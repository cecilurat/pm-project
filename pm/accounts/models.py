from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User, Permission, Group


class Setting(models.Model):
	emergency = models.CharField(max_length = 20, null = True, blank = True)
	routine = models.CharField(max_length = 20, null = True, blank = True)
	others = models.CharField(max_length = 20, null = True, blank = True)
	outstart = models.CharField(max_length = 20, null = True, blank = True) 
	inprocess = models.CharField(max_length = 20, null = True, blank = True)
	finished = models.CharField(max_length = 20, null = True, blank = True)
	rejected = models.CharField(max_length = 20, null = True, blank = True)
	distance = models.CharField(max_length = 20, null = True, blank = True)
	automatic = models.CharField(max_length = 1, null = True, blank = True)
	obligatory = models.CharField(max_length = 1, null = True, blank = True)
	notification = models.CharField(max_length = 1, null = True, blank = True)
	questsup = models.CharField(max_length = 250, default = " ")
	questtech = models.CharField(max_length = 250, default = " ")
	valor1 = models.CharField(max_length = 3, default = " ")
	valor2 = models.CharField(max_length = 3, default = " ")
	valor3 = models.CharField(max_length = 3, default = " ")

class TypeUser(models.Model):
	code = models.CharField(max_length = 2)
	name = models.CharField(max_length = 20)
	status = models.CharField(max_length = 1, null = True, blank = True)
	def __str__(self):
		return self.code

class UserAPP(models.Model):
	username = models.CharField(max_length=50, null = True, blank = True)
	password = models.CharField(max_length=50, null = True, blank = True)
	first_name = models.CharField(max_length=50, null = True, blank = True)
	last_name = models.CharField(max_length=50, null = True, blank = True)
	email = models.CharField(max_length = 50, null = True, blank = True)
	type_user = models.CharField(max_length = 1, null = True, blank = True)
	code = models.CharField(max_length = 50, null = True, blank = True)
	center = models.CharField(max_length = 50, null  = True, blank = True)
	status = models.CharField(max_length = 1, null = True, blank = True)
	last_login = models.CharField(max_length = 40, null = True, blank = True)
	centers = models.CharField(max_length = 1500, null = True, blank = True)
	typeAPP = models.CharField(max_length = 8, null = True, default = " ", blank = True)
	version = models.CharField(max_length = 4, null = True, default = " ", blank = True)
	def __str__(self):
		return self.username + " | " + self.first_name + " | " + self.last_name

class UserPermissions(models.Model):
	code = models.CharField(max_length = 3, null = True, blank = True)
	name = models.CharField(max_length = 20, null = True, blank = True)
	link = models.CharField(max_length = 100, null = True, blank = True)
	result = models.CharField(max_length = 25, null = True, blank = True)
	def __str__(self):
		return self.code + " | "+ self.name
