from rest_framework import serializers
from django.contrib.auth.models import User, Permission, Group
from .models import *
from django.contrib.auth.models import User
from rest_framework import generics


class LoginUserSerializer(serializers.ModelSerializer):
	class Meta:
		model = UserAPP
		fields = ('username', 'password')

class UserAPPSerializer(serializers.ModelSerializer):
	class Meta:
		model = UserAPP
		fields = '__all__'

class UserSerializer(serializers.ModelSerializer):
	class Meta:
		model = User
		fields = '__all__'	


class PermissionSerializer(serializers.ModelSerializer):
	class Meta:
		model = Permission
		fields = '__all__'

class SettingSerializer(serializers.ModelSerializer):
	class Meta:
		model = Setting
		fields = '__all__'

class TypeUserSerializer(serializers.ModelSerializer):
	class Meta:
		model = TypeUser
		fields = '__all__'

class UserPermissionsSerializer(serializers.ModelSerializer):
	class Meta:
		model = UserPermissions
		fields = '__all__'

class UserSettingSerializer(serializers.ModelSerializer):
	obligatory = serializers.SerializerMethodField('is_obligatory')
	permissions = serializers.SerializerMethodField('is_permissions')
	def is_obligatory(self, item):
		return Setting.objects.all().first().obligatory
	def is_permissions(self, item):
		permissions = UserPermissions.objects.filter(code = item.id)
		serializer = UserPermissionsSerializer(permissions, many = True)
		return serializer.data
	class Meta:
		model = User
		fields = '__all__'