from django.conf.urls import url
from accounts.views import *
from pm.settings import LOGOUT_REDIRECT_URL
from django.contrib.auth.views import logout
from .views import *

urlpatterns = [
	url(r'^userapp/$', UserAPPAPIListView.as_view(), name="list_user"),
	url(r'^userapp/(?P<pk>[0-9]+)/$', UserAPPDetailAPIView.as_view(), name="detail_user"),
	url(r'^userapp/password/$', ChangePassword.as_view(), name="change_password"),
	url(r'^user/center/(?P<pk>[0-9]+)/$', UserCenterAPIView.as_view(), name = "user_centers"),
	url(r'^userapp/password/hybrid/$', ChangePassHybrid.as_view(), name="password_hybrid"),

	url(r'^user/$', UserCreateAPIView.as_view(), name="list_user"),
	url(r'^reset/$', ResetPassword.as_view(), name="reset_password"),
	url(r'^user/(?P<pk>[0-9]+)/$', UserDetailAPIView.as_view(), name="detail_user"),
	url(r'^logout/', Logout.as_view()),
	url(r'get/user/$', LoginAPPAPIView.as_view(), name="login"),

	url(r'^loginapp/$', LoginAPPAPIView.as_view(), name="login"),
    url(r'^setting/$', SettingAPIView.as_view(), name="list_setting"),
	url(r'^setting/(?P<pk>[0-9]+)/$', SettingDetailAPIView.as_view(), name="detail_user_setting"),
	url(r'^get/setting/$', SettingListAPI.as_view(), name="get_setting"),
	url(r'^type_user/$', TypeUserList.as_view(), name="type_user"),
	url(r'^type_user/(?P<pk>[0-9]+)/$', TypeUserDetail.as_view(), name="type_user"),
]

