#/usr/bin/env python
# -*- coding: utf-8 -*-
from django.contrib.auth.models import User, Permission
from django.contrib.auth import authenticate
from django.shortcuts import render
from django.http import HttpRequest
from rest_framework.views import APIView
from django.template import RequestContext
from django.http import HttpResponse
from .models import *
from main.models import Center
from main.serializer import CenterSerializer
import json
import datetime
from rest_framework_jwt.settings import api_settings
from django.forms.models import model_to_dict
from django.contrib.auth.models import User, Permission, Group
from django.contrib.auth.decorators import login_required
from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView
from rest_framework.response import Response
from .serializer import *
from datetime import datetime
from rest_framework.permissions import IsAuthenticated
from rest_framework.authtoken.models import Token
from rest_framework.authtoken.views import ObtainAuthToken


#User
class TypeUserList(ListCreateAPIView):
	serializer_class = TypeUserSerializer
    	def get_queryset(self):
		return TypeUser.objects.all()

class TypeUserDetail(RetrieveUpdateDestroyAPIView):
	queryset = TypeUser.objects.all()
	serializer_class = TypeUserSerializer

#Setting
class SettingAPIView(ListCreateAPIView):
	serializer_class = SettingSerializer
	def get_queryset(self):
		return Setting.objects.all()

class SettingDetailAPIView(RetrieveUpdateDestroyAPIView):
	serializer_class = SettingSerializer
	queryset = Setting.objects.all()

class SettingListAPI(APIView):
	def get(self, request, format = None):
		s = Setting.objects.all().first()
		serializer = SettingSerializer(s)
		return Response(serializer.data)

#User APP
class LoginAPPAPIView(APIView):
	def post(self, request, format=None):
		username = request.data.get("username")
		password = request.data.get("password")
		try:
			_user = UserAPP.objects.get(username=username)
		except:
			content = {'response': 'Usuario incorrecto'}
			return Response(content)
		if _user is not None:
			if _user.password != password:
				content = {'response': 'Usuario/clave incorrecta, porfavor intente nuevamente', 'data': 'Usuario/clave incorrecta'}
				return Response(content)
			_user.last_login = datetime.now()
			try:
				_user.typeAPP = request.data.get("typeAPP")
				_user.version = request.data.get("version")
			except:
				pass
			_user.save()
			serializer = UserAPPSerializer(_user)
			content = {'response':'success' ,'data':serializer.data}
			return Response(content)
		else:
			content = {'response':'error' ,'data':'Usuario/clave incorrecta, porfavor intente nuevamente'}
			return Response(content)


class UserAPPAPIListView(ListCreateAPIView):
	serializer_class = UserAPPSerializer
	def get_queryset(self):
		return UserAPP.objects.all()

class UserAPPDetailAPIView(RetrieveUpdateDestroyAPIView):
	serializer_class = UserAPPSerializer
	queryset = UserAPP.objects.all()

class ChangePassword(APIView):
	def post(self, request, format=None):
		id = request.data.get('id')
		password = request.data.get('password')
		try:
			user = UserAPP.objects.get(pk = id)
			user.password = password
			user.save()
			content = {'response': 'success'}
		except:
			content = {'response': 'error'}
		return Response(content)

class UserCenterAPIView(APIView):
	def get(self, request, pk, format=None):
		user = UserAPP.objects.get(pk = pk)
		try:
			_centers = user.centers.split(',')
			centers = Center.objects.filter(WERKS__in = _centers)
			serializer = CenterSerializer(centers, many=True)
			return Response(serializer.data)
		except:
			return Response([])


class UserCreateAPIView(APIView):
	def post(self, request, format=None):
		_user = request.data.get("data")
		permissions = request.data.get("permissions")
		user = User.objects.create_user(username = _user["username"], 
			email = _user["email"], 
			password = _user["password"],
			first_name = _user["first_name"],
			last_name = _user["last_name"],
			is_active = True)
		user.save()
		token = Token.objects.create(user=user)
		for p in permissions:
			permission = UserPermissions(code = user.id, name=p["name"], link=p["link"], result=p["result"])
			permission.save()
		content = {'response': 'success'}
		return Response(content)
	def get(self, request, format=None):
		users = User.objects.all()
		serializer = UserSettingSerializer(users, many = True)
		return Response(serializer.data)


class UserDetailAPIView(APIView):
	def get(self, request, pk, format = None):
		user = User.objects.get(pk = pk)
		serializer = UserSettingSerializer(user)
		return Response(serializer.data)
	def delete(self, request, pk, format = None):
		u = User.objects.get(pk = pk)
		u.delete()
		return Response({'response': 'success'})
	def put(self, request, pk, format = None):
		u = User.objects.get(pk = pk)
		u.username = request.data.get("username")
		u.email = request.data.get("email")
		u.first_name = request.data.get("first_name")
		u.last_name = request.data.get("last_name")
		u.save()
		_permissions = UserPermissions.objects.filter(code = pk)
		setting = Setting.objects.all().first()
		permissions = request.data.get("permissions")
		for _p in _permissions:
			_p.delete()
		for p in permissions:
			permission = UserPermissions(code = u.id, 
				name=p["name"], 
				link=p["link"], 
				result=p["result"])
			permission.save()
		return Response({'response': 'success'})

class UserAuthToken(ObtainAuthToken):
	def post(self, request, *args, **kwargs):
		serializer = self.serializer_class(data=request.data, context={'request': request})
		serializer.is_valid(raise_exception=True)
		user = serializer.validated_data['user']
		token, created = Token.objects.get_or_create(user=user)
		serializer = UserSettingSerializer(user)
		return Response(serializer.data)

class ResetPassword(APIView):
	def post(self, request, format=None):
		pk = request.data.get("id")
		password = request.data.get("password")
		user = User.objects.get(pk = pk)
		user.set_password(password)
		user.save()
		content = {'response': 'success'}
		return Response(content)

class Logout(APIView):
    def get(self, request, format=None):
        request.user.auth_token.delete()
       	content = {'response': 'success'}
       	return Response(content)

class ChangePassHybrid(APIView):
	def post(self, request, format=None):
		id = request.data.get("id")
		new = request.data.get("password")
		old = request.data.get("old")
		try:
			user = UserAPP.objects.get(pk = id)
			if user.password != old:
				return Response({'response': 'Contraseña antigua incorrecta'})
			else:
				user.password = new
				user.save()
				return Response({'response': 'Contraseña actualizada'})
		except:
			return Response({'response': 'Datos incorrectos'})
