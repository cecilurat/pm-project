
from django.contrib import admin
from .models import *

# Register your models here.

#admin.site.register(NoticeType)
#admin.site.register(OTclosing)
admin.site.register(Ticket)
#admin.site.register(COTOperations)
admin.site.register(COTcomponent)
admin.site.register(COTmeasures)
#admin.site.register(OToperations)
admin.site.register(OTcomponent)
admin.site.register(OTmeasures)
admin.site.register(ResponseTicket)
admin.site.register(Log)
admin.site.register(LogDetail)
admin.site.register(ResponseOT)
admin.site.register(SendWeb)
admin.site.register(JobPosition)
admin.site.register(Map)

class OTheadAdmin(admin.ModelAdmin):
	search_fields = ('AUFNR',)

class OToperationsAdmin(admin.ModelAdmin):
	search_fields = ('AUFNR',)

class OTclosingAdmin(admin.ModelAdmin):
	search_fields = ('AUFNR',)

class COTOperationsAdmin(admin.ModelAdmin):
	search_fields = ('AUFNR',)

admin.site.register(OThead, OTheadAdmin)
admin.site.register(OToperations, OToperationsAdmin)
admin.site.register(OTclosing, OTclosingAdmin)
admin.site.register(COTOperations, COTOperationsAdmin)
