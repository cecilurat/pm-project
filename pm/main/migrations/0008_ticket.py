# -*- coding: utf-8 -*-
# Generated by Django 1.9 on 2019-01-26 18:29
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0007_delete_ticket'),
    ]

    operations = [
        migrations.CreateModel(
            name='Ticket',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('TICNR', models.CharField(blank=True, max_length=12, null=True, verbose_name=b'Numero de Orden')),
                ('QMART', models.CharField(blank=True, max_length=2, null=True, verbose_name=b'Tipo de Aviso')),
                ('NQMART', models.CharField(blank=True, max_length=40, null=True, verbose_name=b'Nombre de tipo de Aviso')),
                ('WERKS', models.CharField(blank=True, max_length=4, null=True, verbose_name=b'Center')),
                ('NWERKS', models.CharField(blank=True, max_length=40, null=True, verbose_name=b'Nombre de Centro')),
                ('TPLNR', models.CharField(blank=True, max_length=40, null=True, verbose_name=b'Location')),
                ('NTPLNR', models.CharField(blank=True, max_length=40, null=True, verbose_name=b'Nombre de Locacion')),
                ('EQUNR', models.CharField(blank=True, max_length=18, null=True, verbose_name=b'Equipment')),
                ('NEQUNR', models.CharField(blank=True, max_length=40, null=True, verbose_name=b'Nombre de equipo')),
                ('NDATE', models.CharField(blank=True, max_length=10, null=True, verbose_name=b'Fecha Aviso')),
                ('NTIME', models.CharField(blank=True, max_length=10, null=True, verbose_name=b'Tiempo aviso')),
                ('CODEGRUPPE', models.CharField(blank=True, max_length=8, null=True, verbose_name=b'Sintoma - Codigo grupo')),
                ('CODE', models.CharField(blank=True, max_length=6, null=True, verbose_name=b'Sintoma - Codigo sintoma')),
                ('NCODE', models.CharField(blank=True, max_length=40, null=True, verbose_name=b'Nombre de Sintoma')),
                ('SAP', models.CharField(blank=True, max_length=255, null=True, verbose_name=b'Respuesta de SAP')),
                ('REASON', models.CharField(blank=True, max_length=255, null=True, verbose_name=b'Texto de cause')),
                ('STOPPED', models.CharField(blank=True, max_length=1, null=True, verbose_name=b'Equipo parado')),
                ('STATUS', models.CharField(blank=True, max_length=1, null=True, verbose_name=b'Status')),
                ('APPROVAL', models.CharField(default=b'0', max_length=1, verbose_name=b'Approval')),
                ('CREATED', models.DateTimeField(auto_now_add=True, verbose_name=b'Creacion')),
            ],
            options={
                'verbose_name': 'WTickets',
                'verbose_name_plural': 'WTickets',
            },
        ),
    ]
