# -*- coding: utf-8 -*-
# Generated by Django 1.9 on 2019-02-01 17:41
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0013_auto_20190201_1729'),
    ]

    operations = [
        migrations.AddField(
            model_name='map',
            name='CENTER',
            field=models.CharField(blank=True, max_length=20, null=True, verbose_name=b'Centro'),
        ),
        migrations.AlterField(
            model_name='map',
            name='OBJID',
            field=models.CharField(blank=True, max_length=20, null=True, verbose_name=b'ID de objeto'),
        ),
    ]
