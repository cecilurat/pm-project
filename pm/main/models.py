from django.db import models
from django.core.validators import MaxValueValidator

class Center(models.Model):
	WERKS = models.CharField("Codigo de Centro", max_length = 4, null = True)
	NAME1 = models.CharField("Descripcion de Centro",max_length = 60, null = True)
	BUKRS = models.CharField("Sociedad", max_length = 4, null = True)
	CREATED = models.DateTimeField("Creacion", auto_now_add=True)
	LASTUPDATED = models.DateTimeField("Ultima actualizacion", auto_now=True)

class OrderClass(models.Model):
	AUART = models.CharField("Codigo de Orden de Clase", max_length = 4, null=True)
	TXT = models.CharField("Descripcion de Orden de Clase",max_length = 40, null = True)
	CREATED = models.DateTimeField("Creacion", auto_now_add=True)
	LASTUPDATED = models.DateTimeField("Ultima actualizacion", auto_now=True)	

class Location(models.Model):
	TPLNR = models.CharField("Codigo de ubicacion tecnica", max_length = 40, null = True)
	PLTXT = models.CharField("Descripcion de Orden de Clase",max_length = 40, null = True)
	SWERK = models.CharField("Centro de Emplazamiento", max_length = 4, null = True)
	CREATED = models.DateTimeField("Creacion", auto_now_add=True)
	LASTUPDATED = models.DateTimeField("Ultima actualizacion", auto_now=True)	

class Equipment(models.Model):
	EQUNR = models.CharField("Codigo de equipo", max_length = 18, null = True)
	OBJNR = models.CharField("Numero de objeto", max_length = 22, null = True)
	ABCKZ = models.CharField("Indicador ABC", max_length = 2, null = True)
	RBNR = models.CharField("Perfil de catalogo", max_length = 9, null = True)
	TPLNR = models.CharField("Ubicacion tecnica", max_length = 40, null = True)
	SWERK = models.CharField("Centro de Emplazamiento", max_length = 4, null = True)
	STORT = models.CharField("Emplazamiento", max_length = 10, null = True)
	EQKTX = models.CharField("Descripcion del equipo", max_length = 80, null = True)
	CREATED = models.DateTimeField("Creacion", auto_now_add=True)
	LASTUPDATED = models.DateTimeField("Ultima actualizacion", auto_now=True)	

class Planning(models.Model):
	INGRP = models.CharField("Codigo de Planificacion", max_length = 3, null = True)
	INNAM = models.CharField("Descripcion de Planificacion", max_length = 18, null = True)
	CREATED = models.DateTimeField("Creacion", auto_now_add=True)
	LASTUPDATED = models.DateTimeField("Ultima actualizacion", auto_now=True)	

class Priority(models.Model):
	PRIOK = models.CharField("Codigo de Prioridad", max_length = 1, null = True)
	PRIOKX = models.CharField("Descripcion de Prioridad", max_length = 20, null= True)
	CREATED = models.DateTimeField("Creacion", auto_now_add=True)
	LASTUPDATED = models.DateTimeField("Ultima actualizacion", auto_now=True)	

class GroupPlanCenter(models.Model):
	INGRP = models.CharField("Codigo Grupo de Planificacion", max_length = 3, null = True)
	IWERK = models.CharField("Codigo Centro de Planificacion", max_length = 4, null = True)

class JobPosition(models.Model):
	OBJID = models.CharField("Codigo de Puesto de trabajo", max_length = 8, null = True)
	ARBPL = models.CharField("Puesto de trabajo", max_length = 8, null = True)
	WERKS = models.CharField("Center", max_length = 4, null = True)
	KTEXT = models.CharField("Descripcion del puesto de trabajo", max_length = 40, null = True)
	CREATED = models.DateTimeField("Creacion", auto_now_add=True)
	LASTUPDATED = models.DateTimeField("Ultima actualizacion", auto_now=True)	
	class Meta:
		verbose_name = 'SJobPosition'
		verbose_name_plural = 'SJobPosition'
	def __str__(self):
		return self.OBJID + ' | ' +self.ARBPL

class Measurement(models.Model):
	POINT = models.CharField("Codigo de punto de medida", max_length = 12, null = True)
	MPOBJ = models.CharField("Objecto de punto de medida", max_length = 22, null = True)
	ATINN = models.CharField("Caracterisitca interna", max_length = 30, null = True)
	PTTXT = models.CharField("Descripcion", max_length = 40, null = True)
	CREATED = models.DateTimeField("Creacion", auto_now_add=True)
	LASTUPDATED = models.DateTimeField("Ultima actualizacion", auto_now=True)

class Symptom(models.Model):
	RBNR = models.CharField("Perfil de catalogo", max_length = 9, null = True)
	CODE = models.CharField("Codigo de Sign", max_length = 4, null = True)
	CODEGRUPPE = models.CharField("Grupo de codigos", max_length = 8, null = True)
	VERSION = models.CharField("Version", max_length = 6, null = True)
	INAKTIV = models.CharField("Inactivo", max_length = 1, null = True, blank=True)
	KURZTEXT = models.CharField("Descripcion", max_length = 40, null = True)
	CREATED = models.DateTimeField("Creacion", auto_now_add=True)
	LASTUPDATED = models.DateTimeField("Ultima actualizacion", auto_now=True)

class OThead(models.Model):
	AUFNR = models.CharField("Numero de Orden", max_length = 12, null = True, blank = True)
	AUART = models.CharField("Clase de Orden", max_length = 4, null = True, blank = True)
	INGRP = models.CharField("Grupo de Planificacion", max_length = 3, null = True, blank = True)
	IWERK = models.CharField("Centro de Planificacion", max_length= 4, null = True, blank = True)
	VAPLZ = models.CharField("Puesto de Trabajo", max_length = 8, null = True, blank = True)
	WAWRK = models.CharField("Centro de Puesto de Trabajo", max_length = 4, null = True, blank = True)
	OBJID = models.CharField("ID del Objeto del Pto de Trabajo", max_length = 8, null = True, blank = True)
	OBJID2 = models.CharField("ID del Objeto del Pto de Trabajo 2", max_length = 8, null = True, blank = True)
	SOWRK = models.CharField("Centro de Emplazamiento", max_length = 4, null = True, blank = True)
	KTEXT = models.CharField("Texto de Orden", max_length = 80, null = True, blank = True)
	GSTRP = models.DateField("Fecha de Inicio extrema", max_length = 10, null = True, blank = True)
	GLTRP = models.DateField("Fecha de Fin Extrema", max_length = 10, null = True, blank = True)
	BEGTI = models.CharField("Hora de inicio", max_length = 10, null = True, blank = True)
	ENDTI = models.CharField("Hora fin", max_length = 10, null = True, blank = True)
	NOTNR = models.CharField("N.Aviso Referencia", max_length = 12, null = True, blank = True)
	PRIOK = models.CharField("Prioridad", max_length = 1, null = True, blank = True)
	TPLNR = models.CharField("Ubicacion tecnica", max_length = 40, null = True, blank = True)
	EQUNR = models.CharField("Equipo", max_length = 18, null = True, blank = True)
	NOTIT = models.CharField("Notificacion Tecnico", max_length = 1, default = "0")
	NOTIC = models.CharField("Notificacion Cliente", max_length = 1, default = "0")
	OBSI = models.CharField("Insidencias", max_length = 250, default = "")
	CREATED = models.DateTimeField("Creacion", auto_now_add=True)
	LASTUPDATED = models.DateTimeField("Ultima actualizacion", auto_now=True)
	class Meta:
		verbose_name = 'SOT-Head'
		verbose_name_plural = 'SOT-Head'
		index_together = [("AUFNR")]
	def __str__(self):
		return self.AUFNR


class OToperations(models.Model):
	AUFNR = models.CharField("Numero de Orden", max_length = 12, null = True)
	VORNR = models.CharField("Numero de Operaction", max_length = 4, null = True)
	ARBPL = models.CharField("Puesto de trabajo", max_length = 8, null = True)
	WERKS = models.CharField("Centro", max_length = 4, null = True)
	OBJID = models.CharField("ID Objeto", max_length = 8, null = True)
	STEUS = models.CharField("Clave de Control", max_length = 4, null = True)
	LTXA1 = models.CharField("Texto breve", max_length = 80, null = True)
	BEGDAOP = models.CharField("Fecha inicio", max_length = 10, null = True)
	BEGTIOP = models.CharField("Hora inicio", max_length = 10, null = True)
	ENDDAOP = models.CharField("Fecha fin", max_length = 10, null = True)
	ENDTIOP = models.CharField("Hora fin", max_length = 10, null = True)
	SOWRK = models.CharField("Centro de Emplazamiento", max_length = 4, null = True, blank = True)
	DONE = models.CharField("Hecho ", max_length = 1, default = "")
	OPOBS = models.CharField("Observacion", max_length = 40, default="")
	OPTIME = models.CharField("Valor", max_length = 40, default= "")
	CREATED = models.DateTimeField("Creacion", auto_now_add=True)
	LASTUPDATED = models.DateTimeField("Ultima actualizacion", auto_now=True)
	class Meta:
		verbose_name = 'SOT-Tasks'
		verbose_name_plural = 'SOT-Task'
	def __str__(self):
		return self.AUFNR + ' | ' + self.VORNR


class COTOperations(models.Model):
	AUFNR = models.CharField("Numero de Orden", max_length = 12, null = True)
	VORNR = models.CharField("Numero de Operaction", max_length = 4, null = True)
	LTXA1 = models.CharField("Texto de operacion", max_length = 40, null = True, blank = True)
	DONE = models.CharField("Indicador de Trabajo Realizado", max_length = 1, null = True, blank = True)
	OBJID = models.CharField("ID Objeto", max_length = 8, null = True)
	OPOBS = models.CharField("Observacion de Operacion", max_length = 255, null = True, blank = True)
	OPTIME = models.CharField("Tiempo requerido para Tarea", max_length=15, null = True, blank = True)
	CREATED = models.DateTimeField("Creacion", auto_now_add=True)
	LASTUPDATED = models.DateTimeField("Ultima actualizacion", auto_now=True)
	class Meta:
		verbose_name = 'WOT-Tasks'
		verbose_name_plural = 'WOT-Tasks'
	def __str__(self):
		return self.AUFNR + ' | ' + self.VORNR


class OTcomponent(models.Model):
	AUFNR = models.CharField("Numero de Orden", max_length = 12, null = True)
	VORNR = models.CharField("Numero de Operaction", max_length = 4, null = True)
	POSNR = models.CharField("Puesto de trabajo", max_length = 4, null = True)
	OBJID = models.CharField("ID Objeto", max_length = 8, null = True)
	MATNR = models.CharField("Numero de material", max_length = 18, null = True)
	MAKTX = models.CharField("Descripcion de material", max_length = 40, null = True)
	MEINS_ISO = models.CharField("UM ISO", max_length = 3, null = True)
	MEINS = models.CharField("Unidad de Medida", max_length = 3, null = True)
	MENGE = models.CharField("Cantidad", max_length = 13, null = True)
	SOWRK = models.CharField("Centro de Emplazamiento", max_length = 4, null = True, blank = True)
	DONE = models.CharField("Hecho ", max_length = 1, null = True, blank = True)
	CPOBS = models.CharField("Observacion", max_length = 40, null = True, blank = True)
	QUANTITY = models.CharField("Valor", max_length = 40, null = True, blank = True)
	CREATED = models.DateTimeField("Creacion", auto_now_add=True)
	LASTUPDATED = models.DateTimeField("Ultima actualizacion", auto_now=True)
	class Meta:
		verbose_name = 'SOT-Component'
		verbose_name_plural = 'SOT-Components'
	def __str__(self):
		return self.AUFNR + ' | '+self.VORNR
	
class COTcomponent(models.Model):
	AUFNR = models.CharField("Numero de Orden", max_length = 12, null = True)
	VORNR = models.CharField("Numero de Operaction", max_length = 4, null = True)
	MAKTX = models.CharField("Codigo", max_length = 40, null = True)
	MATNR = models.CharField("Nombre", max_length = 40, null = True)
	MEINS = models.CharField("MEINS", max_length = 40, null = True)
	MENGE = models.CharField("MENGE", max_length = 40, null = True)
	POSNR = models.CharField("Puesto de trabajo", max_length = 4, null = True)
	OBJID = models.CharField("ID Objeto", max_length = 8, null = True)
	DONE = models.CharField("Indicador de trabajo realizado", max_length = 1, null = True, blank= True)
	CPOBS = models.CharField("Observacion de componente", max_length = 355, null = True, blank = True)
	QUANTITY = models.CharField("Cantidad utlilizada", max_length = 15, null = True, blank = True)
	CREATED = models.DateTimeField("Creacion", auto_now_add=True)
	LASTUPDATED = models.DateTimeField("Ultima actualizacion", auto_now=True)
	class Meta:
		verbose_name = 'WOT-Component'
		verbose_name_plural = 'WOT-Components'
	def __str__(self):
		return self.AUFNR + ' | '+self-VORNR

class OTmeasures(models.Model):
	AUFNR = models.CharField("Numero de Orden", max_length = 12, null = True)
	OBJID = models.CharField("ID Objeto", max_length = 8, null = True)
	POINT = models.CharField("Numero de Punto", max_length = 12, null=True)
	EQUNR = models.CharField("Codigo de equipo", max_length = 18, null = True)
	VALUE = models.CharField("Valor medido", max_length = 13, null = True)
	DATE = models.CharField("Fecha",max_length = 10, null = True)
	TIME = models.CharField("TIempo",max_length = 10, null = True)
	VALUE_DATE = models.CharField("Fecha", max_length = 10, null = True)
	VALUE_TIME = models.CharField("Hora", max_length = 10, null = True)
	TAKEN = models.CharField("Hecho ", max_length = 1, null = True, blank = True)
	PMOBS = models.CharField("Observacion", max_length = 40, null = True, blank = True)
	VALUE = models.CharField("Valor", max_length = 40, null = True, blank = True)
	CREATED = models.DateTimeField("Creacion", auto_now_add=True)
	LASTUPDATED = models.DateTimeField("Ultima actualizacion", auto_now=True)
	class Meta:
		verbose_name = 'SOT-MeasurementPoints'
		verbose_name_plural = 'SOT-MeasurementPoints'
	def __str__(self):
		return self.AUFNR + " | "+ self.OBJID+ " | "+self.EQUNR

class COTmeasures(models.Model):
	AUFNR = models.CharField("Numero de Orden", max_length = 12, null = True, blank = True)
	POINT = models.CharField("Numero de Punto", max_length = 12, null=True, blank = True)
	OBJID = models.CharField("ID Objeto", max_length = 8, null = True, blank = True)
	EQUNR = models.CharField("Codigo Equipo", max_length = 18, null = True, blank = True)
	NEQUNR = models.CharField("Nombre de equipo", max_length = 40, null = True, blank = True)
	TAKEN = models.CharField("Punto de Medida", max_length = 1, null = True, blank = True)
	VALUE = models.CharField("Valor medido", max_length = 50, null = True, blank = True)
	VALUE_DATE = models.CharField("Fecha", max_length = 10, null = True, blank = True)
	VALUE_TIME = models.CharField("Hora", max_length = 10, null = True, blank = True)
	PMOBS = models.CharField("Observacion", max_length = 255, null = True, blank = True)
	CREATED = models.DateTimeField("Creacion", auto_now_add=True)
	LASTUPDATED = models.DateTimeField("Ultima actualizacion", auto_now=True)	
	class Meta:
		verbose_name = 'WOT-MeasurementPoint'
		verbose_name_plural = 'WOT-MeasurementPoints'
	def __str__(self):
		return self.AUFNR + ' | '+ self.POINT + ' | '+self.EQUNR

class NoticeType(models.Model):
	CODE = models.CharField("Codigo", max_length=10, null=True)
	DESCRIPTION = models.CharField("Descripcion", max_length = 250, null = True)
	STATUS = models.CharField("Status", max_length = 1, null = True)
	CREATED = models.DateTimeField("Creacion", auto_now_add=True)
	LASTUPDATED = models.DateTimeField("Ultima actualizacion", auto_now=True)

class Geolocation(models.Model):
	CODE = models.CharField("Codigo", max_length=10, null=True)
	DESCRIPTION = models.CharField("Descripcion", max_length=250, null = True)
	LAT = models.CharField("Latitud", max_length=10, null = True)
	LON = models.CharField("Longitud", max_length=10, null= True)
	CREATED = models.DateTimeField("Creacion", auto_now_add=True)
	LASTUPDATED = models.DateTimeField("Ultima actualizacion", auto_now=True)

class ServiceType(models.Model):
	CODE = models.CharField("Codigo", max_length = 10, null = True)
	DESCRIPTION = models.CharField("Descripcion", max_length = 250, null = True)
	ACTIVE = models.CharField("Activo", max_length = 1, null = True)
	STATUS = models.CharField("Estado", max_length = 1, null = True)
	CREATED = models.DateTimeField("Creacion", auto_now_add=True)
	LASTUPDATED = models.DateTimeField("Ultima actualizacion", auto_now=True)

#Salida 

class Ticket(models.Model):
	TICNR = models.CharField("Numero de Orden", max_length = 12, null = True, blank = True)
	QMART = models.CharField("Tipo de Aviso", max_length = 2, null = True, blank = True)
	NQMART = models.CharField("Nombre de tipo de Aviso", max_length = 40, null = True, blank = True)
	WERKS = models.CharField("Center", max_length = 4, null = True, blank = True)
	NWERKS = models.CharField("Nombre de Centro", max_length = 40, null = True, blank = True)
	TPLNR = models.CharField("Location", max_length = 40, null = True, blank = True)
	NTPLNR = models.CharField("Nombre de Locacion", max_length = 40, null = True, blank = True)
	EQUNR = models.CharField("Equipment", max_length = 18)
	NEQUNR = models.CharField("Nombre de equipo", max_length = 40)
	NDATE = models.DateField("Fecha Aviso", null = True)
	NTIME = models.CharField("Tiempo aviso", max_length = 10, null = True, blank = True)
	CODEGRUPPE = models.CharField("Sintoma - Codigo grupo", max_length = 8, null = True, blank = True)
	CODE = models.CharField("Sintoma - Codigo sintoma", max_length = 6, null = True, blank = True)
	NCODE = models.CharField("Nombre de Sintoma", max_length = 40, null = True, blank = True)
	SAP = models.CharField("Respuesta de SAP", max_length = 255, null = True, blank = True)
	REASON = models.CharField("Texto de cause", max_length = 255, null = True, blank = True)
	STOPPED = models.CharField("Equipo parado", max_length = 1, null = True, blank = True)	
	STATUS = models.CharField("Status", max_length = 1, default= "0")
	APPROVAL = models.CharField("Approval", max_length = 1, default = "0")
	NUDOC = models.CharField("Numero de documento", max_length = 12, default = "", blank = True)
	CREATED = models.DateTimeField("Creacion", auto_now_add=True)
	class Meta:
		verbose_name = 'WTickets'
		verbose_name_plural = 'WTickets'
	def __str__(self):
		return self.TICNR

class OTnotification(models.Model):
	AUFNR = models.CharField("Numero de Orden", max_length = 12, null = True)
	ADATE = models.CharField("Fecha de arribo", max_length = 20, null = True)
	ATIME = models.CharField("Hora de arribo", max_length = 10, null = True)


class OTclosing(models.Model):
	AUFNR = models.CharField("Numero de Orden", max_length = 12, null = True)
	OBJID = models.CharField("ID del Objeto del Pto de Trabajo", max_length = 8, null = True)
	SAP = models.CharField("Respues de SAP", max_length = 500, null = True, blank = True)
	TECHAPP = models.CharField("Cierre de Tecnico", max_length = 1, null = True)
	CUSAPP = models.CharField("Cierre de Cliente", max_length = 1, null = True)
	OBSH = models.CharField("Observaciones de la Cabecera", max_length = 500, null = True, blank = True)
	OBSUP = models.CharField("Observaciones", max_length = 255, null = True, blank = True)
	BEGDA = models.CharField("Fecha de inicio de Trabajo", max_length = 20, null = True)
	BEGTI = models.CharField("Hora de inicio de Trabajo", max_length = 20, null = True)
	ENDDA = models.CharField("Fecha de fin de Trabajo", max_length = 20, null = True)
	ENDTI = models.CharField("Hora Fin de trabajo", max_length = 20, null = True)
	ADVANCE = models.CharField("Porcentaje de avance", max_length = 4, null = True, blank = True)
	TOTALHRS = models.CharField("Tiempo total de horas", max_length = 10, null = True)
	RESPSUP = models.CharField("Satisfaccion del Cliente", max_length = 250,  default = " ", blank = True)
	RESPTEC = models.CharField("Satisfaccion del Tecnico", max_length = 250,  default = " ", blank = True)
	VALSUP = models.CharField("Valor del Cliente", max_length = 3, default = " ", blank = True)
	VALTEC = models.CharField("Valor del Tecnico", max_length = 3, default = " ", blank = True)
	DATESUP = models.CharField("Fecha de aprobacion del cliente", max_length = 20, default = " ", blank = True)
	TIMESUP = models.CharField("Hora de aprobacion del cliente", max_length = 20, default = " ", blank = True)
	STATUS = models.CharField("Envio a SAP", max_length = 1, default = "0")
	CREATED = models.DateTimeField("Creacion", auto_now_add=True)
	class Meta:
		verbose_name = 'WOT-Head'
		verbose_name_plural = 'WOT-Head'
	def __str__(self):
		return self.AUFNR



class Log(models.Model):
	DATEHOURSTART = models.DateTimeField("Creacion", auto_now_add=True)
	LOG = models.CharField("Codigo de Log", max_length =40, null = True, blank = True)
	MODEL = models.CharField("Tabla", max_length = 60, null = True, blank = True)
	DESCRIPTION = models.CharField("Descripcion", max_length = 250, null = True)
	class Meta:
		verbose_name = 'SLog'
		verbose_name_plural = 'SLog'
	def __str__(self):
		return self.LOG

class LogDetail(models.Model):
	LOG = models.CharField("ID de Log", max_length = 40, null = True, blank = True)
	CONSULT = models.CharField("Error", max_length = 1000, null = True, blank = True)
	class Meta:
		verbose_name = 'SLogDetail'
		verbose_name_plural = 'SLogDetail'
	def __str__(self):
		return self.LOG

class ResponseOT(models.Model):
	AUFNR = models.CharField("Numero de Orden", max_length = 12, null = True, blank = True)
	MSGTYPE = models.CharField("Tipo de mensaje", max_length = 1, null = True, blank = True)
	MSGDCNR1 = models.CharField("Documento 1", max_length = 20, null = True, blank = True)
	MSGDCNR2 = models.CharField("Documento 2", max_length = 20, null = True, blank = True)
	MSGDCNR3 = models.CharField("Documento 3", max_length = 20, null = True, blank = True)
	MSGDETAIL = models.CharField("Mensaje", max_length = 255, null = True, blank = True)
	class Meta:
		verbose_name = 'WOT-Response'
		verbose_name_plural = 'WOT-Response'
	def __str__(self):
		return self.AUFNR	

class ResponseTicket(models.Model):
	TICNR = models.CharField("Numero de Ticket", max_length = 10, null = True, blank = True)
	MSGTYPE = models.CharField("Tipo de mensaje", max_length = 1, null = True, blank = True)
	MSGDCNR = models.CharField("Documento de aviso", max_length = 20, null = True, blank = True)
	MSGDETAIL = models.CharField("Mensaje", max_length = 255, null = True, blank = True)
	class Meta:
		verbose_name = 'WTicket-Response'
		verbose_name_plural = 'WTicket-Response'
	def __str__(self):
		return self.TICNR

class SendWeb(models.Model):
	DATE = models.CharField("Fecha", max_length = 8, null = True, blank = True)
	OTS = models.CharField("OT recibidas", max_length = 3, null = True, blank = True)
	OTE = models.CharField("OT error", max_length = 3, null = True, blank = True)
	OTC = models.CharField("OT closing", max_length = 3, null = True, blank = True)
	TIC = models.CharField("Ticket", max_length = 3, null = True, blank = True)

class Map(models.Model):
	OBJID = models.CharField("ID de Objeto", max_length = 8, null = True, blank = True)
	DATETIME = models.DateTimeField("Hora y Fecha", auto_now_add = True)
	LAT = models.CharField("Latitud", max_length = 20, null = True, blank = True)
	LON = models.CharField("Longitud", max_length = 20, null = True, blank = True)
	CENTER = models.CharField("Centro", max_length = 20, null = True, blank = True)
	class Meta:
		verbose_name = "WMap"
		verbose_name_plural = "WMaps"
