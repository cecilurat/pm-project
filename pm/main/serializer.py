from rest_framework import serializers
from .models import *

#Center
class CenterSerializer(serializers.ModelSerializer):
	class Meta:
		model = Center
		fields = '__all__'

class CenterDefineSerializer(serializers.ModelSerializer):
	NAME1 = serializers.SerializerMethodField("is_name1")
	def is_name1(self, item):
		return item.WERKS + "-" + item.NAME1
	class Meta:
		model = Center
		fields = ("WERKS","NAME1","BUKRS")

class CenterNameSerializer(serializers.ModelSerializer):
	class Meta:
		model = Center
		fields = ("WERKS","NAME1","BUKRS")	

class CenterAdminSerializer(serializers.ModelSerializer):
	class Meta:
		model = Center
		fields = ("id", "NAME1", "WERKS")

class CenterGeolocationSerializer(serializers.ModelSerializer):
	LAT = serializers.SerializerMethodField("is_latitude")
	LON = serializers.SerializerMethodField("is_longitude")
	DESCRIPTION = serializers.SerializerMethodField("is_description")
	def is_latitude(self, item):
		geolocation = Geolocation.objects.get(CODE = item.WERKS) 
		return geolocation.LAT
	def is_longitude(self, item):
		geolocation = Geolocation.objects.get(CODE = item.WERKS) 
		return geolocation.LON
	def is_description(self, item):
		geolocation = Geolocation.objects.get(CODE = item.WERKS) 
		return geolocation.DESCRIPTION
	class Meta:
		model = Center
		fields = ("WERKS","NAME1","BUKRS","LAT","LON","DESCRIPTION")

#OrderClass
class OrderClassSerializer(serializers.ModelSerializer):
	class Meta:
		model = OrderClass
		fields = '__all__'

#Location
class LocationSerializer(serializers.ModelSerializer):
	class Meta:
		model = Location
		fields = '__all__'

#Equipment
class EquipmentSerializer(serializers.ModelSerializer):
	class Meta:
		model = Equipment
		fields = '__all__'

#Planning
class PlanningSerializer(serializers.ModelSerializer):
	class Meta:
		model = Planning
		fields = '__all__'

class PlanningMobileSerializer(serializers.ModelSerializer):
	NAME = serializers.SerializerMethodField('is_name')
	def is_name(self, item):
		return item.INGRP+"-"+item.INNAM
	class Meta:
		model = Planning
		fields = ("INGRP", "NAME")

#Priority
class PrioritySerializer(serializers.ModelSerializer):
	class Meta:
		model = Priority
		fields = '__all__'

#GroupPlanCenter
class GroupPlanCenterSerializer(serializers.ModelSerializer):
	class Meta:
		model = GroupPlanCenter
		fields = '__all__'

#JobPosition
class JobPositionSerializer(serializers.ModelSerializer):
	class Meta:
		model = JobPosition
		fields = '__all__'

class JobPositionMobilitySerializer(serializers.ModelSerializer):
	class Meta:
		model = JobPosition
		fields = ('id', 'OBJID', 'KTEXT')


#Measurement
class MeasurementSerializer(serializers.ModelSerializer):
	class Meta:
		model = Measurement
		fields = '__all__'

#Symptom
class SymptomSerializer(serializers.ModelSerializer):
	class Meta:
		model = Symptom
		fields = '__all__'

#NoticeType
class NoticeTypeSerializer(serializers.ModelSerializer):
	class Meta:
		model = NoticeType 
		fields = '__all__'

#Geolocation
class GeolocationSerializer(serializers.ModelSerializer):
	class Meta:
		model = Geolocation 
		fields = '__all__'

#ServiceType
class ServiceTypeSerializer(serializers.ModelSerializer):
	class Meta:
		model = ServiceType 
		fields = '__all__'

#OThead
class OTheadSerializer(serializers.ModelSerializer):
	class Meta:
		model = OThead
		fields = '__all__'

class OTheadMobileSerializer(serializers.ModelSerializer):
	AUART = serializers.SerializerMethodField("is_auart")
	NAUART = serializers.SerializerMethodField("is_nauart")
	INGRP = serializers.SerializerMethodField("is_ingrp")
	NINGRP = serializers.SerializerMethodField("is_ningrp")
	INGRP2 = serializers.SerializerMethodField("is_ingrp2")
	NINGRP2 = serializers.SerializerMethodField("is_ningrp2")
	ARBPL = serializers.SerializerMethodField("is_arbpl")
	NARBPL = serializers.SerializerMethodField("is_narbpl")
	WERKS = serializers.SerializerMethodField("is_werks")
	NWERKS = serializers.SerializerMethodField("is_nwerks")
	SOWRK = serializers.SerializerMethodField("is_sowrk")
	NSOWRK = serializers.SerializerMethodField("is_nsowrk")
	NPRIOK = serializers.SerializerMethodField("is_npriok")
	TECHAPP = serializers.SerializerMethodField("is_techapp")
	CUSAPP = serializers.SerializerMethodField("is_cusapp")
	TPLNR = serializers.SerializerMethodField("is_tplnr")
	NTPLNR = serializers.SerializerMethodField("is_ntplnr")
	EQUNR = serializers.SerializerMethodField("is_equnr")
	NEQUNR = serializers.SerializerMethodField("is_nequnr")
	LAT = serializers.SerializerMethodField("is_lat")
	LON = serializers.SerializerMethodField("is_lon")
	OBSH = serializers.SerializerMethodField("is_obsh")
	def is_auart(self, item):
		try:
			order = OrderClass.objects.get(AUART = item.AUART)
			return order.AUART
		except:
			return ""
	def is_nauart(self, item):
		try:
			order = OrderClass.objects.get(AUART = item.AUART)
			return order.TXT
		except:
			return ""
	def is_ingrp(self, item):
		try:
			plan1 = Planning.objects.get(INGRP = item.INGRP)
			return plan1.INGRP
		except:
			return ""
	def is_ningrp(self, item):
		try:
			plan1 = Planning.objects.get(INGRP = item.INGRP)
			return plan1.INNAM
		except:
			return ""
	def is_ingrp2(self, item):
		try:
			groupx = GroupPlanCenter.objects.get(IWERK = item.IWERK)
			plan2 = Planning.objects.get(INGRP = groupx.INGRP)
			return plan2.INGRP
		except:
			return ""
	def is_ningrp2(self, item):
		try:
			groupx = GroupPlanCenter.objects.get(IWERK = item.IWERK)
			plan2 = Planning.objects.get(INGRP = groupx.INGRP)
			return plan2.INNAM
		except:
			return ""
	def is_arbpl(self, item):
		try:
			job = JobPosition.objects.get(ARBPL = item.VAPLZ)
			return job.ARBPL
		except:
			return ""
	def is_narbpl(self, item):
		try:
			job = JobPosition.objects.get(ARBPL = item.VAPLZ)
			return job.KTEXT
		except:
			return ""
	def is_werks(self, item):
		try:
			job = JobPosition.objects.get(ARBPL = item.VAPLZ)
			center = Center.objects.get(WERKS = job.WERKS)
			return center.WERKS
		except:
			return ""
	def is_nwerks(self, item):
		try:
			job = JobPosition.objects.get(ARBPL = item.VAPLZ)
			center = Center.objects.get(WERKS = job.WERKS)
			return center.NAME1
		except:
			return ""
	def is_sowrk(self, item):
		try:
			center = Center.objects.get(WERKS = item.SOWRK)
			return center.WERKS
		except:
			return ""
	def is_nsowrk(self, item):
		try:
			center = Center.objects.get(WERKS = item.SOWRK)
			return center.NAME1
		except:
			return ""
	def is_npriok(self, item):
		try:
			pri = Priority.objects.get(PRIOK = item.PRIOK)
			return pri.PRIOKX
		except:
			return ""
	def is_techapp(self, item):
		try:
			otclos = OTclosing.objects.get(AUFNR = item.AUFNR, OBJID = item.OBJID)
			return otclos.TECHAPP
		except:
			return "0"
	def is_cusapp(self, item):
		try:
			otclos = OTclosing.objects.get(AUFNR = item.AUFNR, OBJID = item.OBJID)
			return otclos.CUSAPP
		except:
			return "0"
	def is_tplnr(self, item):
		try:
			loc = Location.objects.get(TPLNR = item.TPLNR)
			return loc.TPLNR
		except:
			return ""
	def is_ntplnr(self, item):
		try:
			loc = Location.objects.get(TPLNR = item.TPLNR)
			return loc.TPTXT
		except:
			return ""
	def is_equnr(self, item):
		try:
			equ = Equipment.objects.get(EQUNR = item.EQUNR)
			return equ.EQUNR
		except:
			return ""
	def is_nequnr(self, item):
		try:
			equ = Equipment.objects.get(EQUNR = item.EQUNR)
			return equ.EQKTX
		except:
			return ""
	def is_lat(self, item):
		try:
			cent = Center.objects.get(WERKS = item.SOWRK)
			geo = Geolocation.objects.get(CODE = cent.WERKS)
			return geo.LAT
		except:
			return ""
	def is_lon(self, item):
		try:
			cent = Center.objects.get(WERKS = item.SOWRK)
			geo = Geolocation.objects.get(CODE = cent.WERKS)
			return geo.LON
		except:
			return ""
	def is_obsh(self, item):
		try:
			otclos = OTclosing.objects.get(AUFNR = item.AUFNR, OBJID = item.OBJID)
			return otclos.OBSH
		except:
			return ""
	class Meta:
		model = OThead
		fields = ("AUART", "NAUART", "INGRP", "NINGRP", "INGRP2", "NINGRP2", "ARBPL",
			"NARBPL", "IWERK", "OBJID", "WERKS", "NWERKS", "SOWRK", "NSOWRK", "KTEXT",
			"GSTRP", "GLTRP", "BEGTI", "ENDTI", "NOTNR", "PRIOK", "NPRIOK", "TECHAPP",
			"CUSAPP", "TPLNR", "NTPLNR", "EQUNR", "NEQUNR", "LAT", "LON", "OBSH",
			"OBJID2", "CREATED", "OBSI")

class OTheadResumeSerializer(serializers.ModelSerializer):
	NSOWRK = serializers.SerializerMethodField('is_nsowrk')
	GSTRP = serializers.SerializerMethodField('is_gstrp')
	GLTRP = serializers.SerializerMethodField('is_gltrp')
	NTPLNR = serializers.SerializerMethodField('is_ntplnr')
	NEQUNR = serializers.SerializerMethodField('is_nequnr')
	NARBPL = serializers.SerializerMethodField('is_narbpl')
	NINGRP = serializers.SerializerMethodField('is_ningrp')
	NPRIOK = serializers.SerializerMethodField('is_npriok')
	def is_nsowrk(self, item):
		try:
			center = Center.objects.get(WERKS = item.SOWRK)
			return center.NAME1
		except:
			return ""
	def is_gstrp(self, item):
		try:
			otc = OTclosing.objects.get(AUFNR = item.AUFNR)
			return otc.BEGDA
		except:
			return item.GSTRP
	def is_gltrp(self, item):
		try:
			otc = OTclosing.objects.get(AUFNR = item.AUFNR)
			return otc.ENDDA
		except:
			return item.GLTRP
	def is_ntplnr(self, item):
		try:
			loc = Location.objects.get(TPLNR = item.TPLNR)
			return loc.TPTXT
		except:
			return ""
	def is_nequnr(self, item):
		try:
			equ = Equipment.objects.get(EQUNR = item.EQUNR)
			return equ.EQKTX
		except:
			return ""
	def is_narbpl(self, item):
		try:
			job = JobPosition.objects.get(ARBPL = item.VAPLZ)
			return job.KTEXT
		except:
			return ""
	def is_ningrp(self, item):
		try:
			plan1 = Planning.objects.get(INGRP = item.INGRP)
			return plan1.INNAM
		except:
			return ""
	def is_npriok(self, item):
		try:
			pri = Priority.objects.get(PRIOK = item.PRIOK)
			return pri.PRIOKX
		except:
			return ""
	class Meta:
		model = OThead
		fields = ("AUFNR", "NOTNR", "NSOWRK",
			"GSTRP", "GLTRP", "NTPLNR", "NEQUNR", 
			"NARBPL", "NINGRP", "NPRIOK", "OBJID",
			"KTEXT")

class OTheadNotificationSerializer(serializers.ModelSerializer):
	DATE = serializers.SerializerMethodField('is_date')
	TIME = serializers.SerializerMethodField('is_time')
	NPRIOK = serializers.SerializerMethodField('is_npriok')
	def is_date(self, item):
		_date = str(item.CREATED).split('T')
		return _date[0].split(' ')[0]
	def is_time(self, item):
		_date = str(item.CREATED).split('T')
		return _date[0].split(' ')[1]
	def is_npriok(self, item):
		try:
			return Priority.objects.get(PRIOK = item.PRIOK).PRIOKX
		except:
			return ""
	class Meta:
		model = OThead
		fields = ("AUFNR", "OBJID", "DATE",
			"TIME", "NPRIOK")

class OTheadAdminSerializer(serializers.ModelSerializer):
	NAUART = serializers.SerializerMethodField('is_nauart')
	NINGRP = serializers.SerializerMethodField('is_ningrp')
	INGRP2 = serializers.SerializerMethodField('is_ingrp2')
	NINGRP2 = serializers.SerializerMethodField('is_ningrp2')
	ARBPL = serializers.SerializerMethodField('is_arbpl')
	NARBPL = serializers.SerializerMethodField('is_narbpl')
	WERKS = serializers.SerializerMethodField('is_werks')
	NWERKS = serializers.SerializerMethodField('is_nwerks')
	NSOWRK = serializers.SerializerMethodField('is_nsowrk')
	NPRIOK = serializers.SerializerMethodField('is_npriok')
	TECHAPP = serializers.SerializerMethodField('is_techapp')
	CUSAPP = serializers.SerializerMethodField('is_cusapp')
	NTPLNR = serializers.SerializerMethodField('is_ntplnr')
	NEQUNR = serializers.SerializerMethodField('is_nequnr')
	LAT = serializers.SerializerMethodField('is_lat')
	LON = serializers.SerializerMethodField('is_lon')
	OBSH = serializers.SerializerMethodField('is_obsh')
	OBSUP = serializers.SerializerMethodField('is_obsup')
	def is_nauart(self, item):
		try:
			return OrderClass.objects.get(AUART = item.AUART).TXT
		except:
			return ""
	def is_ningrp(self, item):
		try:
			return Planning.objects.get(INGRP = item.INGRP).INNAM
		except:
			return ""
	def is_ingrp2(self, item):
		try:
			groupx = GroupPlanCenter.objects.get(IWERK = item.IWERK)
			return Planning.objects.get(INGRP = groupx.INGRP).INGRP
		except:
			return ""
	def is_ningrp2(self, item):
		try:
			groupx = GroupPlanCenter.objects.get(IWERK = item.IWERK)
			return Planning.objects.get(INGRP = groupx.INGRP).INNAM
		except:
			return ""
	def is_arbpl(self, item):
		try:
			return JobPosition.objects.get(ARBPL = item.VAPLZ).ARBPL
		except:
			return ""
	def is_narbpl(self, item):
		try:
			return JobPosition.objects.get(ARBPL = item.VAPLZ).KTEXT
		except:
			return ""
	def is_werks(self, item):
		try:
			job = JobPosition.objects.get(ARBPL = item.VAPLZ)
			return Center.objects.get(WERKS = job.WERKS).WERKS
		except:
			return ""
	def is_nwerks(self, item):
		try:
			job = JobPosition.objects.get(ARBPL = item.VAPLZ)
			return Center.objects.get(WERKS = job.WERKS).NAME1
		except:
			return ""
	def is_nsowrk(self, item):
		try:
			job = JobPosition.objects.get(ARBPL = item.VAPLZ)
			return Center.objects.get(WERKS = job.WERKS).NAME1
		except:
			return ""
	def is_npriok(self, item):
		try:
			return Priority.objects.get(PRIOK = item.PRIOK).PRIOKX
		except:
			return ""
	def is_techapp(self, item):
		try:
			return OTclosing.objects.get(AUFNR = item.AUFNR, OBJID = item.OBJID).TECHAPP
		except:
			return "0"
	def is_cusapp(self, item):
		try:
			return OTclosing.objects.get(AUFNR = item.AUFNR, OBJID = item.OBJID).CUSAPP
		except:
			return "0"
	def is_ntplnr(self, item):
		try:
			return Location.objects.get(TPLNR = item.TPLNR).TPTXT
		except:
			return ""
	def is_nequnr(self, item):
		try:
			return Equipment.objects.get(EQUNR = item.EQUNR).EQKTX
		except:
			return ""
	def is_lat(self, item):
		try:
			cent = Center.objects.get(WERKS = item.SOWRK)
			return Geolocation.objects.get(CODE = cent.WERKS).LAT
		except:
			return ""
	def is_lon(self, item):
		try:
			cent = Center.objects.get(WERKS = item.SOWRK)
			return Geolocation.objects.get(CODE = cent.WERKS).LON
		except:
			return ""
	def is_obsh(self, item):
		try:
			return OTclosing.objects.get(AUFNR = item.AUFNR, OBJID = item.OBJID).OBSH
		except:
			return ""
	def is_obsup(self, item):
		try:
			return OTclosing.objects.get(AUFNR = item.AUFNR, OBJID = item.OBJID).OBSUP
		except:
			return ""
	class Meta:
		model = OThead
		fields = ("AUFNR", "AUART", "NAUART",
			"INGRP", "NINGRP", "INGRP2", 
			"NINGRP2", "ARBPL", "NARBPL",
			"IWERK", "OBJID", "WERKS",
			"NWERKS", "SOWRK", "NSOWRK",
			"KTEXT", "GSTRP", "GLTRP",
			"BEGTI", "ENDTI", "NOTNR",
			"PRIOK", "NPRIOK", "TECHAPP",
			"CUSAPP", "TPLNR", "NTPLNR",
			"EQUNR", "NEQUNR", "LAT",
			"LON", "OBSH", "OBSUP",
			"OBJID2")

class OTheadReportSerializer(serializers.ModelSerializer):
	UPDATE = serializers.SerializerMethodField('is_update')
	UPTIME = serializers.SerializerMethodField('is_uptime')
	ORDER = serializers.SerializerMethodField('is_order')
	GROUP = serializers.SerializerMethodField('is_group')
	WERKS = serializers.SerializerMethodField('is_werks')
	VAPLZ = serializers.SerializerMethodField('is_vaplz')
	WAWRK = serializers.SerializerMethodField('is_wawrk')
	TPLNR = serializers.SerializerMethodField('is_tplnr')
	EQUNR = serializers.SerializerMethodField('is_equnr')
	COUNTOPER = serializers.SerializerMethodField('is_countoper')
	COUNTCOM = serializers.SerializerMethodField('is_countcom')
	COUNTMEA = serializers.SerializerMethodField('is_countmea')
	COUNTTEC = serializers.SerializerMethodField('is_counttec')
	ADVANCE = serializers.SerializerMethodField('is_advance')
	CLOSEDATE = serializers.SerializerMethodField('is_closedate')
	CLOSETIME = serializers.SerializerMethodField('is_closetime')
	def is_update(self, item):
		return str(item.LASTUPDATED).split('T')[0].split(' ')[0]
	def is_uptime(self, item):
		return str(item.LASTUPDATED).split('T')[0].split(' ')[1]
	def is_order(self, item):
		return item.AUART +"-"+OrderClass.objects.get(AUART = item.AUART).TXT	
	def is_group(self, item):
		return item.INGRP +"-"+Planning.objects.get(INGRP = item.INGRP).INNAM
	def is_werks(self, item):
		try:
			return item.SOWRK +"-"+Center.objects.get(WERKS = item.SOWRK).NAME1
		except:
			return ""
	def is_vaplz(self, item):
		return item.VAPLZ +"-"+JobPosition.objects.filter(ARBPL = item.VAPLZ).first().KTEXT
	def is_wawrk(self, item):
		return item.WAWRK +"-"+Center.objects.get(WERKS = item.WAWRK).NAME1
	def is_tplnr(self, item):
		try:
			return o.TPLNR + "-"+Location.objects.get(TPLNR = o.TPLNR).PLTXT
		except:
			return ""
	def is_equnr(self, item):
		try:
			return item.EQUNR + "-"+Equipment.objects.get(EQUNR = item.EQUNR).EQKTX
		except:
			return ""
	def is_countoper(self, item):
		return str(len(OToperations.objects.filter(AUFNR = o.AUFNR, OBJID = o.OBJID)))
	def is_countcom(self, item):
		return str(len(OTcomponent.objects.filter(AUFNR = o.AUFNR, OBJID = o.OBJID)))
	def is_countmea(self, item):
		return str(len(OTmeasures.objects.filter(AUFNR = o.AUFNR, OBJID = o.OBJID)))
	def is_counttec(self, item):
		return str(len(OToperations.objects.filter(AUFNR = o.AUFNR, OBJID = o.OBJID).values_list("ARBPL").distinct()))
	def is_advance(self, item):
		try:
			otclos = OTclosing.objects.get(AUFNR = item.AUFNR, OBJID = item.OBJID)
			return "100"
		except:
			return "0"
	def is_closedate(self, item):
		try:
			return OTclosing.objects.get(AUFNR = item.AUFNR, OBJID = item.OBJID).ENDDA
		except:
			return ""
	def is_closetime(self, item):
		try:
			return OTclosing.objects.get(AUFNR = item.AUFNR, OBJID = item.OBJID).ENDTI
		except:
			return ""
	class Meta:
		model = OThead
		fields = ("AUFNR", "UPDATE", "UPTIME",
			"ORDER", "GROUP", "WERKS", "KTEXT",
			"GSTRP", "GLTRP", "VAPLZ", "WAWRK",
			"TPLNR", "EQUNR", "COUNTOPER",
			"COUNTCOM", "COUNTMEA", "COUNTTEC",
			"ADVANCE", "CLOSEDATE", "CLOSETIME")

class OTheadReportTechnicSerializer(serializers.ModelSerializer):
	SEND = serializers.SerializerMethodField('is_send')
	JOBNAME = serializers.SerializerMethodField('is_ktext')
	BEGDA = serializers.SerializerMethodField('is_begda')
	BEGTI = serializers.SerializerMethodField('is_begti')
	ENDDA = serializers.SerializerMethodField('is_endda')
	ENDTI = serializers.SerializerMethodField('is_endti')
	NAME1 = serializers.SerializerMethodField('is_name1')
	PLTXT = serializers.SerializerMethodField('is_pltxt')
	EQKTX = serializers.SerializerMethodField('is_eqktx')
	TIME = serializers.SerializerMethodField('is_time')
	APPR = serializers.SerializerMethodField('is_appr')
	DATESUP = serializers.SerializerMethodField('is_datesup')
	TIMESUP = serializers.SerializerMethodField('is_timesup')
	OBSH = serializers.SerializerMethodField('is_obsh')
	OBSUP = serializers.SerializerMethodField('is_obsup')
	SATTEC = serializers.SerializerMethodField('is_sattec')
	VALTEC = serializers.SerializerMethodField('is_valtec')
	SATSUP = serializers.SerializerMethodField('is_satsup')
	VALSUP = serializers.SerializerMethodField('is_valsup')
	def is_send(self, item):
		otclos = OTclosing.objects.get(AUFNR = item.AUFNR, OBJID = item.OBJID)
		if otclos.STATUS == "0":
			return "No"
		else:
			return "Si"
	def is_ktext(self, item):
		return JobPosition.objects.filter(ARBPL = item.VAPLZ).first().KTEXT
	def is_begda(self, item):
		return OTclosing.objects.get(AUFNR = item.AUFNR, OBJID = item.OBJID).BEGDA
	def is_begti(self, item):
		return OTclosing.objects.get(AUFNR = item.AUFNR, OBJID = item.OBJID).BEGTI
	def is_endda(self, item):
		return OTclosing.objects.get(AUFNR = item.AUFNR, OBJID = item.OBJID).ENDDA
	def is_endti(self, item):
		return OTclosing.objects.get(AUFNR = item.AUFNR, OBJID = item.OBJID).ENDTI
	def is_name1(self, item):
		return Center.objects.get(WERKS = item.IWERK).NAME1
	def is_pltxt(self, item):
		try:
			return Location.objects.get(TPLNR = ot.TPLNR).PLTXT
		except:
			return ""	
	def is_eqktx(self, item):
		try:
			return Equipment.objects.get(EQUNR = ot.EQUNR).EQKTX
		except:
			return ""
	def is_time(self, item):
		otclos = OTclosing.objects.get(AUFNR = item.AUFNR, OBJID = item.OBJID)
		try:
			timini = datetime.strptime( otclos.BEGDA +' '+otclos.BEGTI, '%Y-%m-%d %H:%M:%S')
			timefin = datetime.strptime(otclos.ENDDA +' '+ otclos.ENDTI, '%Y-%m-%d %H:%M:%S')
			return str(timefin - timini)
		except:
			return "00:00:00"
	def is_appr(self, item):
		otclos = OTclosing.objects.get(AUFNR = item.AUFNR, OBJID = item.OBJID)
		if otclos.CUSAPP == "1":
			return "Si"
		else:
			return "No"
	def is_datesup(self, item):
		otclos = OTclosing.objects.get(AUFNR = item.AUFNR, OBJID = item.OBJID)
		if otclos.CUSAPP == "1":
			return otclos.DATESUP
		else:
			return ""
	def is_timesup(self, item):
		otclos = OTclosing.objects.get(AUFNR = item.AUFNR, OBJID = item.OBJID)
		if otclos.CUSAPP == "1":
			return otclos.TIMESUP
		else:
			return ""
	def is_obsh(self, item):
		return OTclosing.objects.get(AUFNR = item.AUFNR, OBJID = item.OBJID).OBSH
	def is_obsup(self, item):
		return OTclosing.objects.get(AUFNR = item.AUFNR, OBJID = item.OBJID).OBSUP
	def is_sattec(self, item):
		otclos = OTclosing.objects.get(AUFNR = item.AUFNR, OBJID = item.OBJID)
		if otclos.VALTEC == "1":
			return "Alegre"
		if otclos.VALTEC == "0":
			return "Triste"
		if otclos.VALTEC == "0.5":
			return "Medio"
		return ""
	def is_valtec(self, item):
		return OTclosing.objects.get(AUFNR = item.AUFNR, OBJID = item.OBJID).RESPTEC
	def is_satsup(self, item):
		otclos = OTclosing.objects.get(AUFNR = item.AUFNR, OBJID = item.OBJID) 
		if otclos.VALSUP == "1":
			return "Alegre"
		if otclos.VALSUP == "0":
			return "Triste"
		if otclos.VALSUP == "0.5":
			return "Medio"
		return ""
	def is_valsup(self, item):
		return OTclosing.objects.get(AUFNR = item.AUFNR, OBJID = item.OBJID).RESPSUP
	class Meta:
		model = OThead
		fields = ("AUFNR", "SEND", "VAPLZ",
			"JOBNAME", "KTEXT", "BEGDA", "BEGTI",
			"ENDDA", "ENDTI", "IWERK",
			"NAME1", "TPLNR", "PLTXT",
			"EQUNR", "EQKTX", "TIME",
			"APPR", "DATESUP", "TIMESUP",
			"OBSH", "OBSUP", "SATTEC",
			"VALTEC", "SATSUP", "VALSUP")

#OToperations
class OToperationsSerializer(serializers.ModelSerializer):
	class Meta:
		model = OToperations
		fields = '__all__'

class OToperationsAdminSerializer(serializers.ModelSerializer):
	DONE = serializers.SerializerMethodField('is_done')
	OPOBS = serializers.SerializerMethodField('is_opobs')
	OPTIME = serializers.SerializerMethodField('is_optime')
	def is_done(self, item):
		try:
			otclos = COTOperations.objects.get(AUFNR = oto.AUFNR, VORNR = oto.VORNR)
			return otclos.DONE
		except:
			return "0"
	def is_opobs(self, item):
		try:
			otclos = COTOperations.objects.get(AUFNR = oto.AUFNR, VORNR = oto.VORNR)
			return otclos.OPOBS
		except:
			return oto.OPOBS
	def is_optime(self, item):
		try:
			otclos = COTOperations.objects.get(AUFNR = oto.AUFNR, VORNR = oto.VORNR)
			return otclos.OPTIME
		except:
			return oto.OPTIME
	class Meta:
		model = OToperations
		fields = ("AUFNR", "VORNR", "ARBPL", 
			"WERKS", "OBJID", "STEUS", "LTXA1", 
			"BEGDAOP", "BEGTIOP",
			"ENDDAOP", "ENDTIOP",
			"SOWRK", "DONE", "OPOBS", "OPTIME")

class COToperationsSerializer(serializers.Serializer):
	id = serializers.CharField(max_length = 10)
	AUFNR = serializers.CharField(max_length = 12)
	VORNR = serializers.CharField(max_length = 4)
	LTXA1 = serializers.CharField(max_length = 40)
	DONE = serializers.CharField(max_length = 1)
	OBJID = serializers.CharField(max_length = 8)
	OPOBS = serializers.CharField(max_length = 255)
	OPTIME = serializers.CharField(max_length = 15)

class COToperationSerializer(serializers.ModelSerializer):
	class Meta:
		model = COTOperations
		fields = '__all__'

class COTOperationsSAPSerializer(serializers.ModelSerializer):
	OPOBS = serializers.SerializerMethodField('is_opobs')
	OPTIME = serializers.SerializerMethodField('is_optime')
	def is_opobs(self, item):
		return item.OPOBS[:250]
	def is_optime(self, item):
		if i.OPTIME == "" or i.OPTIME is None:
			return "0"
		return item.OPTIME
	class Meta:
		model = COTOperations
		fields = ("AUFNR", "VORNR", "OBJID",
			"DONE", "OPOBS", "OPTIME")

class COTOperationsReportSerializer(serializers.ModelSerializer):
	JOBCODE = serializers.SerializerMethodField('is_jobcode')
	JOBNAME = serializers.SerializerMethodField('is_jobname')
	DONE = serializers.SerializerMethodField('is_done')
	OPTIME60 = serializers.SerializerMethodField('is_optime60')
	def is_jobcode(self, item):
		return JobPosition.objects.filter(OBJID = item.OBJID).first().ARBPL
	def is_jobname(self, item):
		return JobPosition.objects.filter(OBJID = item.OBJID).first().KTEXT			
	def is_done(self, item):
		if item.DONE == "1":
			return "Si"
		else:
			return "No"
	def is_optime60(self, item):
		return str(float(item.OPTIME)*60)
	class Meta:
		model = COTOperations
		fields = ("AUFNR", "JOBCODE", "JOBNAME", "VORNR",
			"LTXA1", "DONE", "OPTIME", "OPTIME60",
			"OPOBS")

#OTcomponent
class OTcomponentSerializer(serializers.ModelSerializer):
	class Meta:
		model = OTcomponent
		fields = '__all__'

class OTcomponentAdminSerializer(serializers.ModelSerializer):
	DONE = serializers.SerializerMethodField('is_done')
	CPOBS = serializers.SerializerMethodField('is_cpobs')
	QUANTITY = serializers.SerializerMethodField('is_quantity')
	def is_done(self, item):
		try:
			otclos = COTcomponent.objects.get(AUFNR = otc.AUFNR, VORNR = otc.VORNR)
			return otclos.DONE
		except:
			return "0"
	def is_cpobs(self, item):
		try:
			otclos = COTcomponent.objects.get(AUFNR = otc.AUFNR, VORNR = otc.VORNR)
			return otclos.CPOBS
		except:
			return ""
	def is_quantity(self, item):
		try:
			otclos = COTcomponent.objects.get(AUFNR = otc.AUFNR, VORNR = otc.VORNR)
			return otclos.QUANTITY
		except:
			return ""
	class Meta:
		model = OTcomponent
		fields = ("AUFNR", "VORNR", "POSNR",
			"OBJID", "MATNR", "MAKTX",
			"MEINS_ISO", "MEINS", "SOWRK",
			"DONE", "CPOBS", "QUANTITY", "MENGE")

class COTcomponentSerializer(serializers.Serializer):
	id = serializers.CharField(max_length = 10)
	AUFNR = serializers.CharField(max_length = 12)
	VORNR = serializers.CharField(max_length = 4)
	LTXA1 = serializers.CharField(max_length = 40)
	DONE = serializers.CharField(max_length = 10)
	OBJID = serializers.CharField(max_length = 10)
	OPOBS = serializers.CharField(max_length = 10)
	OPTIME = serializers.CharField(max_length = 10)

class COTcomponenSerializer(serializers.ModelSerializer):
	class Meta:
		model = COTcomponent
		fields = '__all__'

class COTcomponentSAPSerializer(serializers.ModelSerializer):
	CPOBS = serializers.SerializerMethodField('is_cpobs')
	QUANTITY = serializers.SerializerMethodField('is_quantity')
	def is_cpobs(self, item):
		return item.CPOBS[:250]
	def is_quantity(self, item):
		if item.QUANTITY == "" or item.QUANTITY is None:
			return "0"
		return item.QUANTITY
	class Meta:
		model = COTcomponent
		fields = ("AUFNR", "VORNR", "POSNR",
			"OBJID", "DONE", "CPOBS", "QUANTITY")

class COTcomponentReportSerializer(serializers.ModelSerializer):
	JOBNAME = serializers.SerializerMethodField('is_jobname')
	DONE = serializers.SerializerMethodField('is_done')
	def is_jobname(self, item):
		return JobPosition.objects.filter(OBJID = item.OBJID).first().KTEXT
	def is_done(self, item):
		if item.DONE == "1":
			return "Si"
		else:
			return "No"
	class Meta:
		model = COTcomponent
		fields = ("AUFNR", "POSNR", "JOBNAME",
			"MATNR", "MAKTX", "MEINS", 
			"DONE", "MENGE")

#OTmeasures
class OTmeasuresSerializer(serializers.ModelSerializer):
	class Meta:
		model = OTmeasures
		fields = '__all__'

class OTmeasuresAdminSerializer(serializers.ModelSerializer):
	TAKEN = serializers.SerializerMethodField('is_taken')
	PMOBS = serializers.SerializerMethodField('is_pmobs')
	VALUE = serializers.SerializerMethodField('is_value')
	NEQUNR = serializers.SerializerMethodField('is_nequnr')
	def is_taken(self, item):
		try:
			otclos = COTmeasures.objects.get(AUFNR = otm.AUFNR, POINT = otm.POINT)
			return otclos.TAKEN
		except:
			return "0"
	def is_pmobs(self, item):
		try:
			otclos = COTmeasures.objects.get(AUFNR = otm.AUFNR, POINT = otm.POINT)
			return otclos.PMOBS
		except:
			return "None"
	def is_value(self, item):
		try:
			otclos = COTmeasures.objects.get(AUFNR = otm.AUFNR, POINT = otm.POINT)
			return otclos.VALUE
		except:
			return "None"
	class Meta:
		model = OTmeasures
		fields = ("AUFNR", "OBJID", "POINT",
			"EQUNR", "VALUE", "DATE", "TIME",
			"TAKEN", "PMOBS", "VALUE", "NEQUNR")

class OTmeasuresMobileSerializer(serializers.ModelSerializer):
	NEQUNR = serializers.SerializerMethodField("is_nequnr")
	def is_nequnr(self, item):
		try:
			med = Measurement.objects.get(POINT = item.POINT)
			return med.PTTXT
		except:
			return ""
	class Meta:
		model = OTmeasures
		fields = ('AUFNR', 'POINT', 
			'EQUNR', 'NEQUNR', 
			'VALUE', 'DATE', 
			'VALUE_DATE', 'VALUE_TIME',
			'TAKEN', 'PMOBS',
			'VALUE', 'OBJID')

class OTmeasuresGetSerializer(serializers.ModelSerializer):
	class Meta:
		model = OTmeasures
		field = ('id', 'AUFNR', 'OBJID', 'POINT', 'EQUNR', 'VALUE', 'DATE', 'TIME', 'VALUE_DATE', 'VALUE_TIME', 'TAKEN', 'PMOBS', 'VALUE', )


class COTmeasuresSerializer(serializers.Serializer):
	id = serializers.CharField(max_length = 10)
	AUFNR = serializers.CharField(max_length = 12)
	POINT = serializers.CharField(max_length = 12)
	OBJID = serializers.CharField(max_length = 8)
	EQUNR = serializers.CharField(max_length = 18)
	NEQUNR = serializers.CharField(max_length = 40)
	TAKEN = serializers.CharField(max_length = 1)
	VALUE = serializers.CharField(max_length = 50)
	VALUE_DATE = serializers.CharField(max_length = 10)
	VALUE_TIME = serializers.CharField(max_length = 10)
	PMOBS = serializers.CharField(max_length = 255)

class COTmeasureSerializer(serializers.ModelSerializer):
	class Meta:
		model = COTmeasures
		fields = '__all__'

class COTmeasureSAPSerializer(serializers.ModelSerializer):
	PMOBS = serializers.SerializerMethodField('is_pmobs')
	def is_pmobs(self, item):
		return item.PMOBS[:250]
	class Meta:
		model = COTmeasures
		fields = ("AUFNR", "POINT", "OBJID", 
			"EQUNR", "TAKEN", "VALUE", 
			"VALUE_DATE", "VALUE_TIME", "PMOBS")

class COTmeasureReportSerializer(serializers.ModelSerializer):
	JOBNAME = serializers.SerializerMethodField('is_jobname')
	EQURNAME = serializers.SerializerMethodField('is_equrname')
	DONE = serializers.SerializerMethodField('is_done')
	def is_jobname(self, item):
		return JobPosition.objects.filter(OBJID = item.OBJID).first().KTEXT
	def is_equrname(self, item):
		return Equipment.objects.get(EQUNR = item.EQUNR).EQKTX			
	def is_done(self, item):
		if item.VALUE != "":
			return "Si"
		else:
			return "No"
	class Meta:
		model = COTmeasures
		fields = ("AUFNR", "OBJID", "JOBNAME", 
			"EQUNR", "EQURNAME", "POINT",
			"PMOBS", "DONE", "VALUE")

#Ticket
class TicketSerializer(serializers.ModelSerializer):
	class Meta:
		model = Ticket
		fields = '__all__'


#OTnotification
class OTnotificationSerializer(serializers.ModelSerializer):
	class Meta:
		model = OTnotification
		fields = '__all__'

#OTclosing
class OTclosingSerializer(serializers.ModelSerializer):
	class Meta:
		model = OTclosing
		fields = '__all__'

class OTclosingSAPSerializer(serializers.ModelSerializer):
	TOTALHRS = serializers.SerializerMethodField("is_totalhrs")
	def is_totalhrs(self, item):
		return item.TOTALHRS.replace(',','.')
	class Meta:
		model = OTclosing
		fields = ("AUFNR", "OBJID", "TECHAPP",
			"CUSAPP", "OBSH", "BEGTI", "ENDDA",
			"ENDTI", "TOTALHRS")

class OTclosingNotificationSerializer(serializers.ModelSerializer):
	DATE = serializers.SerializerMethodField('is_date')
	TIME = serializers.SerializerMethodField('is_time')
	NPRIOK = serializers.SerializerMethodField('is_npriok')
	def is_date(self, item):
		_date = str(ot.CREATED).split('T')
		return _date[0].split(' ')[0]
	def is_time(self, item):
		_date = str(ot.CREATED).split('T')
		return _date[0].split(' ')[1]
	def is_npriok(self, item):
		try:
			othead = OThead.objects.get(AUFNR = item.AUFNR, OBJID = item.OBJID)
			return Priority.objects.get(PRIOK = othead.PRIOK).PRIOKX
		except:
			return ""
	class Meta:
		model = OTclosing
		fields = ("AUFNR", "OBJID", "SOWRK",
			"DATE", "TIME", "NPRIOK")

#Log
class LogSerializer(serializers.ModelSerializer):
	class Meta:
		model = Log
		fields = '__all__'

class LogDetailSerializer(serializers.ModelSerializer):
	class Meta:
		model = LogDetail
		fields = '__all__'


class LogWebSerializer(serializers.ModelSerializer):
	DETAILS = serializers.SerializerMethodField('is_details')
	def is_details(self, item):
		details = LogDetail.objects.filter(LOG = item.LOG)
		serializer = LogDetailSerializer(details, many = True)
		return serializer.data
	class Meta:
		model = Log
		fields = '__all__'

#Map
class MapSerializer(serializers.ModelSerializer):
	class Meta:
		model = Map
		fields = '__all__'

class MapPositionSerializer(serializers.ModelSerializer):
	NOBJID = serializers.SerializerMethodField('is_nobjid')
	NCENTER = serializers.SerializerMethodField('is_ncenter')
	def is_nobjid(self, item):
		job = JobPosition.objects.get(OBJID = item.OBJID)
		return job.KTEXT
	def is_ncenter(self, item):
		center = Center.objects.get(WERKS = item.CENTER)
		return center.NAME1
	class Meta:
		model = Map
		fields = ("id", "OBJID", "NOBJID",
			"LAT", "LON", "CENTER", "NCENTER")

#OTclosings
class OTSerializer(serializers.Serializer):
	id = serializers.CharField(max_length = 10)
	AUFNR = serializers.CharField(max_length = 12)
	OBJID = serializers.CharField(max_length = 8)
	TECHAPP = serializers.CharField(max_length = 1)
	CUSAPP = serializers.CharField(max_length = 1)
	OBSH = serializers.CharField(max_length = 500)
	OBSUP = serializers.CharField(max_length = 255)
	BEGDA = serializers.CharField(max_length = 20)
	BEGTI = serializers.CharField(max_length = 20)
	ENDDA = serializers.CharField(max_length = 20)
	ENDTI = serializers.CharField(max_length = 20)
	TOTALHRS = serializers.CharField(max_length = 10)
	STATUS = serializers.CharField(max_length = 1)
	ADVANCE = serializers.CharField(max_length = 4)
	SAP = serializers.CharField(max_length = 500)
	ARBPL = serializers.CharField(max_length = 100)
	NARBPL = serializers.CharField(max_length = 100)
	TITLE = serializers.CharField(max_length = 100)
	PLAN = serializers.CharField(max_length = 100)
	CENTER = serializers.CharField(max_length = 100)
	LOC = serializers.CharField(max_length = 100)
	EQUIPO = serializers.CharField(max_length = 100)
	RESPSUP = serializers.CharField(max_length = 255)
	RESPTEC = serializers.CharField(max_length = 255)
	VALSUP = serializers.CharField(max_length = 3)
	VALTEC = serializers.CharField(max_length = 3)
	DATESUP = serializers.CharField(max_length = 20)
	TIMESUP = serializers.CharField(max_length = 20)
	TECHNIC  =  serializers.CharField(max_length = 100)
	UTECH = serializers.CharField(max_length = 100)
	USUP = serializers.CharField(max_length = 100)
	OBSI = serializers.CharField(max_length = 100)
	COTOPERATIONS = COToperationsSerializer(many = True)
	COTCOMPONENT = COTcomponentSerializer(many = True)
	COTMEASURES = COTmeasuresSerializer(many = True)


class GraficSerializer(serializers.Serializer):
	DAY = serializers.CharField(max_length = 20)
	OTC = serializers.CharField(max_length = 10)
	OTE = serializers.CharField(max_length = 10)

#Ticket
class TicketSerializer(serializers.ModelSerializer):
	MSGPRO = serializers.SerializerMethodField('is_msgpro')
	STAT = serializers.SerializerMethodField('is_stat')
	DATE_CREATED = serializers.SerializerMethodField('is_created_date')
	TIME_CREATED = serializers.SerializerMethodField('is_created_time')
	STOPPED = serializers.SerializerMethodField('is_stopped')
	FINI = serializers.SerializerMethodField('is_fini')
	TINI = serializers.SerializerMethodField('is_tini')
	def is_msgpro(self, item):
		if item.STATUS == "1" or item.STATUS == "2":
			return "Si"
		else:
			return "No"
	def is_stat(self, item):
		if item.STATUS == "1":
			return "Enviado"
		if item.STATUS == "0":
			return "Sin enviar"
		if item.STATUS == "2":
			return "Con Error"
	def is_created_date(self, item):
		return str(item.CREATED).split('T')[0].split(' ')[0]
	def is_created_time(self, item):
		return str(item.CREATED).split('T')[0].split(' ')[1]
	def is_stopped(self, item):
		if item.STOPPED == "1":
			return "Parado"
		else:
			return "En Funcionamiento"
	def is_fini(self, item):
		if item.STATUS != "1":
			return ""
		else:
			return item.NDATE
	def is_tini(self, item):
		if item.STATUS != "1":
			return ""
		else:
			return item.NTIME
	class Meta:
		model = Ticket
		fields = '__all__'