from spyne import Application, rpc, Mandatory as M, Unicode, UnsignedInteger32, \
    Array, Iterable, TTableModel, Service

from spyne import Application, rpc, ServiceBase, Iterable, Integer, Unicode, String
import requests
import json
from zeep.transports import Transport
from sqlalchemy import create_engine
from sqlalchemy import MetaData
from sqlalchemy.orm import sessionmaker
from spyne.protocol.http import HttpRpc
from spyne.protocol.json import JsonDocument
from spyne.protocol.xml import XmlDocument
from spyne.protocol.soap import Soap11, Soap12
from spyne.server.wsgi import WsgiApplication
from datetime import datetime

db = create_engine('sqlite:///:memory:')
Session = sessionmaker(bind=db)
TableModel = TTableModel()
TableModel.Attributes.sqla_metadata.bind = db
serv = 'http://52.41.46.242:443'

class Center(TableModel):
    __tablename__ = 'center'
    __namespace__ = 'pm.soap.service.center'
    __table_args__ = {"sqlite_autoincrement": True}

    id = UnsignedInteger32(primary_key=True)
    WERKS = String()
    NAME1 = String()
    BUKRS = String()


class OrderClass(TableModel):
    __tablename__ = 'orden'
    __namespace__ = 'pm.soap.service.orden'
    __table_args__ = {"sqlite_autoincrement": True}

    id = UnsignedInteger32(primary_key=True)
    AUART = String()
    TXT = String()

class Location(TableModel):
    __tablename__ = 'location'
    __namespace__ = 'pm.soap.service.location'
    __table_args__ = {"sqlite_autoincrement": True}

    id = UnsignedInteger32(primary_key=True)
    TPLNR = String()
    PLTXT = String()
    SWERK = String()

class Equipment(TableModel):
    __tablename__ = 'equipment'
    __namespace__ = 'pm.soap.service.equipment'
    __table_args__ = {"sqlite_autoincrement": True}

    id = UnsignedInteger32(primary_key=True)
    EQUNR = String()
    OBJNR = String()
    ABCKZ = String()
    RBNR = String()
    TPLNR = String()
    SWERK = String()   
    STORT = String()
    EQKTX = String()

class Planning(TableModel):
    __tablename__ = 'planning'
    __namespace__ = 'pm.soap.service.planning'
    __table_args__ = {"sqlite_autoincrement": True}

    id = UnsignedInteger32(primary_key=True)
    INGRP = String()
    INNAM = String()

class Priority(TableModel):
    __tablename__ = 'priority'
    __namespace__ = 'pm.soap.service.priority'
    __table_args__ = {"sqlite_autoincrement": True}

    id = UnsignedInteger32(primary_key=True)
    PRIOK = String()
    PRIOKX = String()

class JobPosition(TableModel):
    __tablename__ = 'position'
    __namespace__ = 'pm.soap.service.position'
    __table_args__ = {"sqlite_autoincrement": True}

    id = UnsignedInteger32(primary_key=True)
    OBJID = String()
    ARBPL = String()
    WERKS = String()
    KTEXT = String()

class Measurement(TableModel):
    __tablename__ = 'measurement'
    __namespace__ = 'pm.soap.service.measurement'
    __table_args__ = {"sqlite_autoincrement": True}

    id = UnsignedInteger32(primary_key=True)
    POINT = String()
    MPOBJ = String()
    ATINN = String()
    PTTXT = String()

class Symptom(TableModel):
    __tablename__ = 'symptom'
    __namespace__ = 'pm.soap.service.symptom'
    __table_args__ = {"sqlite_autoincrement": True}

    id = UnsignedInteger32(primary_key=True)
    CODE = String()
    RBNR = String()
    CODEGRUPPE = String()
    VERSION = String()
    INAKTIV = String()
    KURZTEXT = String()

class OThead(TableModel):
    __tablename__ = 'othead'
    __namespace__ = 'pm.soap.service.othead'
    __table_args__ = {"sqlite_autoincrement": True}

    id = UnsignedInteger32(primary_key=True)
    AUFNR = String()
    AUART = String()
    INGRP = String()
    IWERK = String()
    VAPLZ = String()
    WAWRK = String()
    OBJID = String()
    SOWRK = String()
    KTEXT = String()
    GSTRP = String()
    GLTRP = String()
    BEGTI = String()
    ENDTI = String()
    NOTNR = String()
    PRIOK = String()
    TPLNR = String()
    EQUNR = String()
    
class OToperations(TableModel):
    __tablename__ = 'otoperations'
    __namespace__ = 'pm.soap.service.otoperations'
    __table_args__ = {"sqlite_autoincrement": True}

    id = UnsignedInteger32(primary_key=True)
    AUFNR = String()
    VORNR = String()
    ARBPL = String()
    WERKS = String()
    OBJID = String()
    STEUS = String()
    LTXA1 = String()
    BEGDAOP = String()
    BEGTIOP = String()
    ENDDAOP = String()
    ENDTIOP = String()

class OTcomponent(TableModel):
    __tablename__ = 'otcomponent'
    __namespace__ = 'pm.soap.service.otcomponent'
    __table_args__ = {"sqlite_autoincrement": True}

    id = UnsignedInteger32(primary_key=True)
    AUFNR = String()
    VORNR = String()
    POSNR = String()
    MATNR = String()
    OBJID = String()
    MAKTX = String()
    MEINS_ISO = String()
    MEINS = String()
    MENGE = String()

class OTmeasures(TableModel):
    __tablename__ = 'otmeasures'
    __namespace__ = 'pm.soap.service.otmeasures'
    __table_args__ = {"sqlite_autoincrement": True}

    id = UnsignedInteger32(primary_key=True)
    AUFNR = String()
    POINT = String()
    EQUNR = String()
    VALUE = String()
    DATE = String()
    TIME = String()

class GroupPlanCenter(TableModel):
    __tablename__ = 'groupplancenter'
    __namespace__ = 'pm.soap.service.groupplancenter'
    __table_args__ = {"sqlite_autoincrement": True}

    id = UnsignedInteger32(primary_key=True)
    INGRP = String()
    IWERK = String()

class SoapService(ServiceBase):
    @rpc(Array(Center, store_as='table'), _returns=Iterable(Unicode))
    def set_center(ctx, center):
        true = 0
        elem = len(center)
        r = requests.delete(serv + '/api/main/center/delete/')
        for i in center:
            werks = str(i.WERKS)
            name1 = str(i.NAME1)
            bukrs = str(i.BUKRS)
            r = requests.post(serv + '/api/main/center/' , json={"WERKS": werks,"NAME1": name1,"BUKRS": bukrs}) 
            codUnique = "log" + "center" +str(datetime.now()) 
            fly = 0
            if str(r) == '<Response [201]>':
                true = true + 1
            else:
                log_detail = requests.post(serv + '/api/main/log/detail/', json={"LOG": codUnique, "CONSULT": "WERKS: "+werks+ ", NAME1: "+name1+", BUKRS: "+bukrs})
                fly = 1                
        yield 'Total registros: ' +  str(elem) + ', insertados:' +str(true) + ', error: ' + str(elem - true)         
        if fly == 1:
            log = requests.post(serv + '/api/main/log/', json={"LOG": codUnique, "MODEL": "Center", "DESCRIPTION": "Total registros: " +  str(elem) + ", insertados:" +str(true) + ", error: " + str(elem - true)})


    @rpc(Array(OrderClass, store_as='table'), _returns=Iterable(Unicode))
    def set_order_class(ctx, orden):
        true = 0
        elem = len(orden)
        r = requests.delete(serv + '/api/main/orderclass/delete/')
        for i in orden:
            codUnique = "log" + "orderclass" +str(datetime.now()) 
            fly = 0
            auart = str(i.AUART)
            txt = str(i.TXT)
            r = requests.post(serv + '/api/main/orderclass/', json={"AUART": auart,"TXT": txt})   
            if str(r) == '<Response [201]>':
                true = true + 1
            else:
                log_detail = requests.post(serv + '/api/main/log/detail/', json={"LOG": codUnique, "CONSULT": "AUART :" +auart+ ", TXT:" +txt + " "})
                fly = 1  
        yield 'Total registros: ' +  str(elem) + ', insertados:' +str(true) + ', error: ' + str(elem - true)
        if fly == 1:
            log = requests.post(serv + '/api/main/log/', json={"LOG": codUnique, "MODEL": "Order Class", "DESCRIPTION": "Total registros: " +  str(elem) + ", insertados:" +str(true) + ", error: " + str(elem - true)})            

    @rpc(Array(Location, store_as='table'), _returns=Iterable(Unicode))
    def set_location(ctx, location):
        true = 0 
        elem = len(location)
        r = requests.delete(serv + '/api/main/location/delete/')
        for i in location:
            codUnique = "log" + "location" +str(datetime.now()) 
            fly = 0
            tplnr = str(i.TPLNR)
            pltxt = str(i.PLTXT)
            swerk = str(i.SWERK)
            r = requests.post(serv + '/api/main/location/', json={"TPLNR": tplnr,"PLTXT": pltxt, "SWERK": swerk})
            if str(r) == '<Response [201]>':
                true = true + 1 
            else:
                log_detail = requests.post(serv + '/api/main/log/detail/', json={"LOG": codUnique, "CONSULT": "TPLNR: "+ tplnr +" ,PLTXT:"+ pltxt +", SWERK:"+ swerk + " "})
                fly = 1  
        yield 'Total registros: ' +  str(elem) + ', insertados:' +str(true) + ', error: ' + str(elem - true)
        if fly == 1:
            log = requests.post(serv + '/api/main/log/', json={"LOG": codUnique, "MODEL": "Location", "DESCRIPTION": "Total registros: " +  str(elem) + ", insertados:" +str(true) + ", error: " + str(elem - true)})            

    @rpc(Array(Equipment, store_as='table'), _returns=Iterable(Unicode))
    def set_equipment(ctx, equipment):
        true = 0
        elem = len(equipment)
        r = requests.delete(serv + '/api/main/equipment/delete/')
        elem = len(equipment)
        for i in equipment:
            codUnique = "log" + "equipment" +str(datetime.now()) 
            fly = 0
            equnr = str(i.EQUNR)
            objnr = str(i.OBJNR)
            abckz = str(i.ABCKZ)
            rbnr = str(i.RBNR)
            tplnr = str(i.TPLNR)
            swerk = str(i.SWERK)   
            stort = str(i.STORT)  
            eqktx = str(i.EQKTX)
            r = requests.post(serv + '/api/main/equipment/', json={"EQUNR": equnr,"OBJNR": objnr, "ABCKZ": abckz, "RBNR": rbnr, "TPLNR": tplnr, "SWERK": swerk, "STORT": stort, "EQKTX": eqktx})   
            if str(r) == '<Response [201]>':
                true = true + 1
            else:
                log_detail = requests.post(serv + '/api/main/log/detail/', json={"LOG": codUnique, "CONSULT": "EQUNR: " +equnr+", OBJNR: " +objnr +", ABCKZ: " +abckz+ ", RBNR: "+rbnr +", TPLNR: "+tplnr +", SWERK: "+swerk +", STORT: "+ stort+", EQKTX: "+eqktx + " "})
                fly = 1  
        yield 'Total registros: ' +  str(elem) + ', insertados:' +str(true) + ', error: ' + str(elem - true)
        if fly == 1:
            log = requests.post(serv + '/api/main/log/', json={"LOG": codUnique, "MODEL": "Equipment", "DESCRIPTION": "Total registros: " +  str(elem) + ", insertados:" +str(true) + ", error: " + str(elem - true)})


    @rpc(Array(Planning, store_as='table'), _returns=Iterable(Unicode))
    def set_planning(ctx, planning):
        true = 0
        elem = len(planning)
        r = requests.delete(serv + '/api/main/planning/delete/')
        for i in planning:
            codUnique = "log" + "planning" +str(datetime.now()) 
            fly = 0
            ingrp = str(i.INGRP)
            innam = str(i.INNAM)
            r = requests.post(serv + '/api/main/planning/', json={"INGRP": ingrp,"INNAM": innam})   
            if str(r) == '<Response [201]>':
                true = true + 1
            else:
                log_detail = requests.post(serv + '/api/main/log/detail/', json={"LOG": codUnique, "CONSULT": "INGRP: "+ ingrp +"INNAM :" +innam + " "})
                fly = 1  
        yield 'Total registros: ' +  str(elem) + ', insertados:' +str(true) + ', error: ' + str(elem - true)
        if fly == 1:
            log = requests.post(serv + '/api/main/log/', json={"LOG": codUnique, "MODEL": "Planning", "DESCRIPTION": "Total registros: " +  str(elem) + ", insertados:" +str(true) + ", error: " + str(elem - true)})

    @rpc(Array(Priority, store_as='table'), _returns=Iterable(Unicode))
    def set_priority(ctx, priority):
        true = 0
        elem = len(priority)
        r = requests.delete(serv + '/api/main/priority/delete/')
        for i in priority:
            codUnique = "log" + "priority" +str(datetime.now()) 
            fly = 0
            priok = str(i.PRIOK)
            priokx = str(i.PRIOKX)
            r = requests.post(serv + '/api/main/priority/', json={"PRIOK": priok,"PRIOKX": priokx})   
            if str(r) == '<Response [201]>':
                true = true + 1
            else:
                log_detail = requests.post(serv + '/api/main/log/detail/', json={"LOG": codUnique, "CONSULT": "PRIOK: " +priok +", PRIOKX:"+ priokx + " "})
                fly = 1  
        yield 'Total registros: ' +  str(elem) + ', insertados:' +str(true) + ', error: ' + str(elem - true)
        if fly == 1:
            log = requests.post(serv + '/api/main/log/', json={"LOG": codUnique, "MODEL": "Priority", "DESCRIPTION": "Total registros: " +  str(elem) + ", insertados:" +str(true) + ", error: " + str(elem - true)})

    @rpc(Array(JobPosition, store_as='table'), _returns=Iterable(Unicode))
    def set_jobposition(ctx, jobposition):
        true = 0
        elem = len(jobposition)
        r = requests.delete(serv + '/api/main/jobposition/delete/')
        for i in jobposition:
            codUnique = "log" + "jobposition" +str(datetime.now()) 
            fly = 0
            objid = str(i.OBJID)
            arbpl = str(i.ARBPL)
            werks = str(i.WERKS)
            ktext = str(i.KTEXT)
            r = requests.post(serv + '/api/main/jobposition/', json={"OBJID": objid,"ARBPL": arbpl,"WERKS": werks,"KTEXT": ktext})   
            if str(r) == '<Response [201]>':
                true = true + 1
            else:
                log_detail = requests.post(serv + '/api/main/log/detail', json={"LOG": codUnique, "DESCRIPTION": "PRIOK: " +priok +", PRIOKX:"+ priokx + " "})
                fly = 1  
        yield 'Total registros: ' +  str(elem) + ', insertados:' +str(true) + ', error: ' + str(elem - true)
        if fly == 1:
            log = requests.post(serv + '/api/main/log/', json={"LOG": codUnique, "MODEL": "Job Position", "DESCRIPTION": "Total registros: " +  str(elem) + ", insertados:" +str(true) + ", error: " + str(elem - true)})

    @rpc(Array(Measurement, store_as='table'), _returns=Iterable(Unicode))
    def set_measurement(ctx, measurement):
        true = 0
        elem = len(measurement)
        r = requests.delete(serv + '/api/main/measurement/delete/')
        for i in measurement:
            codUnique = "log" + "measurement" +str(datetime.now()) 
            fly = 0
            point = str(i.POINT)
            mpobj = str(i.MPOBJ)
            atinn = str(i.ATINN)
            pttxt = str(i.PTTXT)
            r = requests.post(serv + '/api/main/measurement/', json={"POINT": point,"MPOBJ": mpobj,"ATINN": atinn,"PTTXT": pttxt})   
            if str(r) == '<Response [201]>':
                true = true + 1
            else:
                log_detail = requests.post(serv + '/api/main/log/detail/', json={"LOG": codUnique, "CONSULT": "POINT:"+ point+", MPOBJ:" +mpobj+", ATINN:"+ atinn+ ", PTTXT:"+ pttxt + " "})
                fly = 1  
        yield 'Total registros: ' +  str(elem) + ', insertados:' +str(true) + ', error: ' + str(elem - true)
        if fly == 1:
            log = requests.post(serv + '/api/main/log/', json={"LOG": codUnique, "MODEL": "Measurement", "DESCRIPTION": "Total registros: " +  str(elem) + ", insertados:" +str(true) + ", error: " + str(elem - true)})

    @rpc(Array(Symptom, store_as='table'), _returns=Iterable(Unicode))
    def set_symptom(ctx, symptom):
        true = 0
        elem = len(symptom)
        r = requests.delete(serv + '/api/main/symptom/delete/')
        for i in symptom:
            codUnique = "log" + "symptom" +str(datetime.now()) 
            fly = 0
            code = str(i.CODE)
            rbnr = str(i.RBNR)
            codegruppe = str(i.CODEGRUPPE)
            version = str(i.VERSION)
            inaktiv = str(i.INAKTIV)
            if i.INAKTIV == None:
		inaktiv = ""  
	   
	    kurztext = str(i.KURZTEXT)
            r = requests.post(serv + '/api/main/symptom/', json={"CODE": code,"RBNR": rbnr,"CODEGRUPPE": codegruppe,"VERSION": version,"INAKTIV": inaktiv,"KURZTEXT": kurztext})   
            if str(r) == '<Response [201]>':
                true = true + 1
            else:
                log_detail = requests.post(serv + '/api/main/log/detail/', json={"LOG": codUnique, "CONSULT": "CODE: " +code +", RBNR:"+ rbnr+ ", CODEGRUPPE:" +codegruppe +",VERSION:" +version+",INAKTIV:" +inaktiv+ ", KURZTEXT: "+ kurztext + " "})
                fly = 1  
        yield 'Total registros: ' +  str(elem) + ', insertados:' +str(true) + ', error: ' + str(elem - true)
        if fly == 1:
            log = requests.post(serv + '/api/main/log/', json={"LOG": codUnique, "MODEL": "Symptom", "DESCRIPTION": "Total registros: " +  str(elem) + ", insertados:" +str(true) + ", error: " + str(elem - true)})

    @rpc(Array(OThead, store_as='table'), Array(OToperations, store_as='table'), Array(OTcomponent, store_as='table'), Array(OTmeasures, store_as='table'), _returns=Iterable(Unicode))
    def set_ot(ctx, othead, otoperations, otcomponent, otmeasures):
        true = 0
        elem = len(othead)
        #r = requests.delete(serv + '/api/main/othead/delete/')
        for i in othead:
            codUnique = "log" + "othead" +str(datetime.now()) 
            fly = 0
            aufnr = str(i.AUFNR)
            auart = str(i.AUART)
            ingrp = str(i.INGRP)
            iwerk = str(i.IWERK)
            vaplz = str(i.VAPLZ)
            wawrk = str(i.WAWRK)
            objid = str(i.OBJID)
            sowrk = str(i.SOWRK)
            ktext = str(i.KTEXT)
            if i.KTEXT == None:
                ktext = ""
	    gstrp = str(i.GSTRP)
            gltrp = str(i.GLTRP)
            begti = str(i.BEGTI)
            endti = str(i.ENDTI)
            notnr = str(i.NOTNR)
	    if i.NOTNR == None:
                notnr = ""
            priok = str(i.PRIOK)
	    if i.PRIOK == None:
		 priok = ""
            tplnr = str(i.TPLNR)
            equnr = str(i.EQUNR)
            res = requests.post(serv + '/api/main/validator/', json={"AUFNR": aufnr, "HEAD": "1"})
            cont = res.content
            if cont[13]  == "0":
                r = requests.post(serv + '/api/main/othead/', json={"AUFNR": aufnr ,"AUART": auart,"INGRP": ingrp,"IWERK": iwerk,"VAPLZ": vaplz,"WAWRK": wawrk,"OBJID": objid,"SOWRK": sowrk, "KTEXT": ktext, "GSTRP":gstrp, "GLTRP": gltrp, "BEGTI":begti, "ENDTI":endti, "NOTNR":notnr, "PRIOK": priok,"TPLNR": tplnr, "EQUNR":equnr, "OBJID2":objid })   
                if str(r) == '<Response [201]>' or str(r) == '<Response [200]':
                    true = true + 1
                    r = requests.post(serv + '/api/main/otnotification/', json={"AUFNR": aufnr,"ADATE": "2019" ,"ATIME": "2019" })
                else:
                    log_detail = requests.post(serv + '/api/main/log/detail/', json={"LOG": codUnique, "CONSULT": "AUFNR: "+ aufnr +", AUART: "+ auart +", INGRP: "+ingrp+ ", IWERK: "+ iwerk+ ", VAPLZ:"+ vaplz+", WAWRK: "+wawrk+", OBJID: "+objid+" , SOWRK: "+sowrk+", KTEXT:"+ktext+", GSTRP: "+gstrp+", GLTRP:"+gltrp+", BEGTI: "+begti+", ENDTI:"+endti+", NOTNR: "+notnr+", PRIOK:"+priok+", TPLNR:"+tplnr+ ", EQUNR:"+ equnr+", OBJID2:"+objid })
                    fly = 1
        yield 'Total registros: ' +  str(elem) + ', insertados:' +str(true) + ', error: ' + str(elem - true)
        if fly == 1:
            send = requests.post(serv + '/api/main/get/data/', json={"OTE": str(elem - true)})
            log = requests.post(serv + '/api/main/log/', json={"LOG": codUnique, "MODEL": "OThead", "DESCRIPTION": "Total registros: " +  str(elem) + ", insertados:" +str(true) + ", error: " + str(elem - true) })

        true = 0
        elem = len(otoperations)
        #r = requests.delete(serv + '/api/main/otoperations/delete/')
        for i in otoperations:  
            codUnique = "log" + "otoperations" +str(datetime.now()) 
            fly = 0
            aufnr = str(i.AUFNR)
            yornr = str(i.VORNR)
            arbpl = str(i.ARBPL)
            werks = str(i.WERKS)
            objid = str(i.OBJID)
            steus = str(i.STEUS)
            ltxa1 = str(i.LTXA1)
            begdaop = str(i.BEGDAOP)
            begtiop = str(i.BEGTIOP)
            enddaop = str(i.ENDDAOP)
            endtiop = str(i.ENDTIOP)
            res = requests.post(serv + '/api/main/validator/', json={"AUFNR": aufnr, "HEAD": "0"})
            cont = res.content
	    print(cont[13])
            if cont[13]  == "0":
                r = requests.post(serv + '/api/main/otoperations/', json={"AUFNR": aufnr,"VORNR": yornr,"ARBPL": arbpl,"WERKS": werks,"OBJID": objid,"STEUS": steus,"LTXA1": ltxa1, "BEGDAOP":begdaop, "BEGTIOP":begtiop, "ENDDAOP":enddaop, "ENDTIOP":endtiop})   
                if str(r) == '<Response [201]>' or str(r) == '<Response [200]>':
                    true = true + 1
                else:
                    log_detail = requests.post(serv + '/api/main/log/detail/', json={"LOG": codUnique, "CONSULT": "AUFNR:"+ aufnr+", VORNR:"+yornr+", ARBPL:"+arbpl+", WERKS:"+werks+", OBJID:"+objid+", STEUS:"+steus+", LTXA1:"+ltxa1+", BEGDAOP:"+begdaop+", BEGTIOP:"+begtiop+", ENDDAOP:"+enddaop+", ENDTIOP:"+endtiop+ " "})
                    fly = 1
            else:

		print("no se borro")
	yield 'Total registros: ' +  str(elem) + ', insertados:' +str(true) + ', error: ' + str(elem - true)
        if fly == 1:
            send = requests.post(serv + '/api/main/get/data/', json={"OTE": str(elem - true)})
            log = requests.post(serv + '/api/main/log/', json={"LOG": codUnique, "MODEL": "OToperations", "DESCRIPTION": "Total registros: " +  str(elem) + ", insertados:" +str(true) + ", error: " + str(elem - true) })



        true = 0
        elem = len(otcomponent)
        #r = requests.delete(serv + '/api/main/otcomponent/delete/')
        for i in otcomponent:
            codUnique = "log" + "otcomponent" +str(datetime.now()) 
            fly = 0
            aufnr = str(i.AUFNR)
            vornr = str(i.VORNR)
            posnr = str(i.POSNR)
            objid = str(i.OBJID)
            matnr = str(i.MATNR)
            maktx = str(i.MAKTX)
            meins_iso = str(i.MEINS_ISO)
            meins = str(i.MEINS)
            menge = str(i.MENGE)
            res = requests.post(serv + '/api/main/validator/', json={"AUFNR": aufnr, "HEAD": "0"})

            cont = res.content
	    print(cont[13])
            if cont[13] == "0":

                r = requests.post(serv + '/api/main/otcomponent/', json={"AUFNR": aufnr,"VORNR": yornr,"POSNR": posnr,"MATNR": matnr,"MAKTX": maktx, "OBJID": objid,"MEINS_ISO": meins_iso,"MEINS": meins,"MENGE": menge})   
                if str(r) == '<Response [201]>' or str(r) == '<Response [200]>':
                    true = true + 1
                else:
                    log_detail = requests.post(serv + '/api/main/log/', json={"LOG": codUnique, "CONSULT": "AUFNR:"+aufnr+", VORNR:"+yornr+", POSNR:"+posnr+", MATNR:"+matnr+", MAKTX:"+maktx+", OBJID:"+objid+", MEINS_ISO:"+meins_iso+", MEINS:"+meins+", MENGE:"+menge+ " "})
                    fly = 1
        yield 'Total registros: ' +  str(elem) + ', insertados:' +str(true) + ', error: ' + str(elem - true)
        if fly == 1:
            send = requests.post(serv + '/api/main/get/data/', json={"OTE": str(elem - true)})
            log = requests.post(serv + '/api/main/log/', json={"LOG": codUnique, "MODEL": "OTcomponent", "DESCRIPTION": "Total registros: " +  str(elem) + ", insertados:" +str(true) + ", error: " + str(elem - true) })
        

        true = 0
        elem = len(otmeasures)
        for i in otmeasures:
            codUnique = "log" + "otmeasures" +str(datetime.now()) 
            fly = 0
            aufnr = str(i.AUFNR)
            point = str(i.POINT)
            equnr = str(i.EQUNR)
            value = str(i.VALUE)
            dates = str(i.DATE)
            times = str(i.TIME)
            res = requests.post(serv + '/api/main/validator/', json={"AUFNR": aufnr, "HEAD": "0"})
            cont = res.content
	    print(cont[13])
            if cont[13] ==  "0":
                r = requests.post(serv + '/api/main/otmeasures/', json={ "AUFNR": aufnr,"POINT": point,"EQUNR": equnr,"VALUE": value,"DATE": dates,"TIME": times})   
                if str(r) == '<Response [201]' or str(r) == '<Response [200]>' :
                    true = true + 1
                else:
                    log_detail = requests.post(serv + '/api/main/log/detail/', json={"LOG": codUnique, "CONSULT": "AUFNR:"+aufnr+",VORNR:"+yornr+", POSNR:"+posnr+", MATNR:"+matnr+", MAKTX:"+maktx+", OBJID:"+objid+", MEINS_ISO:"+meins_iso+", MEINS:"+meins+", MENGE:"+menge+ " "})
                    fly = 1
        yield 'Total registros: ' +  str(elem) + ', insertados:' +str(true) + ', error: ' + str(elem - true)
        if fly == 1:
            send = requests.post(serv + '/api/main/get/data/', json={"OTE": str(elem - true)})
            log = requests.post(serv + '/api/main/log/', json={"LOG": codUnique, "MODEL": "OTmeasures", "DESCRIPTION": "Total registros: " +  str(elem) + ", insertados:" +str(true) + ", error: " + str(elem - true) })


    @rpc(Array(GroupPlanCenter, store_as='table'), _returns=Iterable(Unicode))
    def set_groupplancenter(ctx, groupplancenter):
        true = 0
        elem = len(groupplancenter)
        r = requests.delete(serv + '/api/main/groupplancenter/delete/')
        for i in groupplancenter:
            codUnique = "log" + "groupplancenter" +str(datetime.now()) 
            fly = 0
            ingrp = str(i.INGRP)
            iwerk = str(i.IWERK)
            r = requests.post(serv + '/api/main/groupplancenter/', json={"INGRP": ingrp ,"IWERK": iwerk})   
            if str(r) == '<Response [201]>':
                true = true + 1
            else:
                log_detail = requests.post(serv + '/api/main/log/detail/', json={"LOG": codUnique, "CONSULT": "INGRP:"+ingrp+", IWERK"+ iwerk+ " "})
                fly = 1
        yield 'Total registros: ' +  str(elem) + ', insertados:' +str(true) + ', error: ' + str(elem - true)        
        if fly == 1:
            log = requests.post(serv + '/api/main/log/', json={"LOG": codUnique, "MODEL": "GroupPlanCenter", "DESCRIPTION": "Total registros: " +  str(elem) + ", insertados:" +str(true) + ", error: " + str(elem - true) })

application = Application([SoapService], 'spyne.service.soap',
                          in_protocol=Soap11(validator='lxml'),
                          out_protocol=Soap11())

wsgi_application = WsgiApplication(application)


if __name__ == '__main__':
    import logging

    from wsgiref.simple_server import make_server
    logging.basicConfig(level=logging.DEBUG)
    logging.getLogger('spyne.protocol.xml').setLevel(logging.DEBUG)

    logging.info("listening to http://127.0.0.1:8000")
    logging.info("wsdl is at: http://localhost:8000/?wsdl")

    server = make_server('0.0.0.0', 80, wsgi_application)
    server.serve_forever()
