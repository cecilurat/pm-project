from django.apps import AppConfig


class SpyneConfig(AppConfig):
    name = 'spyne'
