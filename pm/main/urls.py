from django.conf.urls import url
from main.views import *

urlpatterns = [

    url(r'^service/(?P<code>.*)/(?P<date>.*)/$', ServiceAux.as_view(), name="service_aux"),

    url(r'^orderclass/$', OrderClassListCreateAPIView.as_view(), name="list_orderclass"),
    url(r'^orderclass/delete/$', OrderClassDeleteSOAPConection.as_view(), name="delete_orderclass"),
#    url(r'^mobility/get/orderclass/(?P<pk>[0-9]+)/$', OrderClassDeleteView.as_view(), name="delete_orderclass"),

    url(r'^center/$', CenterListCreateAPIView.as_view(), name="list_center"),
    url(r'^get/center/(?P<code>.*)/$', CenterListView.as_view(), name = "get_center"),
    url(r'^center/delete/$', CenterDeleteSOAPConection.as_view(), name="delete_centers"),
    url(r'^mobility/get/center/$', GeolocationCenterAPI.as_view(), name="get_center"),

    url(r'^location/$', LocationListCreateAPIView.as_view(), name="list_location"),
    url(r'^location/delete/$', LocationDeleteSOAPConection.as_view(), name="delete_location"),
    url(r'^mobility/get/location/(?P<pk>.*)/$', LocationGetView.as_view(), name="get_location"),

    url(r'^equipment/$', EquipmentListCreateAPIView.as_view(), name="list_equipment"),
    url(r'^equipment/delete/$', EquipmentDeleteSOAPConection.as_view(), name="delete_equipment"),
    url(r'^equipment/center/(?P<center>.*)/$', EquipmentService.as_view(), name="equipment_filter"),
    url(r'^mobility/get/equipment/(?P<pk>.*)/$', EquipmentGetView.as_view(), name="get_equipment"),

    url(r'^planning/$', PlanningListCreateAPIView.as_view(), name="list_planning"),
    url(r'^planning/delete/$', PlanningDeleteSOAPConection.as_view(), name="delete_planning"),

    url(r'^priority/$', PriorityListCreateAPIView.as_view(), name="list_priority"),
    url(r'^priority/delete/$', PriorityDeleteSOAPConection.as_view(), name="delete_priority"),

    url(r'^groupplancenter/$', GroupPlanCenterListCreateAPIView.as_view(), name="list_groupplancenter"),
    url(r'^groupplancenter/delete/$', GroupPlanCenterDeleteSOAPConection.as_view(), name="delete_groupplancenter"),
    url(r'^get/groupplancenter/$', PlanCenterService.as_view(), name="get_groupplancenter"),
 #   url(r'^groupplancenter/(?P<pk>[0-9]+)/$', GroupPlanCenterUpdateDetailAPIView.as_view(), name="detail_groupplancenter"),

    url(r'^jobposition/$', JobPositionListCreateAPIView.as_view(), name="list_jobposition"),
    url(r'^jobposition/delete/$', JobPositionDeleteSOAPConection.as_view(), name="delete_jobposition"),
#    url(r'^mobility/get/jobposition/$', JobPositionListView.as_view(), name="list_jobposition"),

    url(r'^measurement/$', MeasurementListCreateAPIView.as_view(), name="list_measurement"),
    url(r'^measurement/delete/$', MeasurementDeleteSOAPConection.as_view(), name="delete_measurement"),

    url(r'^notice_type/$', NoticeTypeListCreateAPIView.as_view(), name="list_notice_type"),
    url(r'^notice_type/(?P<pk>[0-9]+)/$', NoticeTypeUpdateDetailAPIView.as_view(), name="detail_notice_type"),
    url(r'^notice_type/delete/$', NoticeTypeDeleteSOAPConection.as_view(), name="delete_notice_type"),
    url(r'^mobility/get/notice_type/$', NoticeTypeListView.as_view(), name="list_notice_type"),

    url(r'^geolocation/$', GeolocationListCreateAPIView.as_view(), name="list_geolocation"),
    url(r'^geolocation/(?P<pk>[0-9]+)/$', GeolocationUpdateDetailAPIView.as_view(), name="detail_geolocation"),
    url(r'^geolocation/delete/$', GeolocationDeleteSOAPConection.as_view(), name="delete_geolocation"),

    url(r'^service_type/$', ServiceTypeListCreateAPIView.as_view(), name="list_service_type"),
    url(r'^service_type/delete/$', ServiceTypeDeleteSOAPConection.as_view(), name="delete_service_type"),

    url(r'^symptom/$', SymptomListCreateAPIView.as_view(), name="list_symptom"),
    url(r'^symptom/delete/$', SymptomDeleteSOAPConection.as_view(), name="delete_symptom"),
    url(r'^mobility/get/symptom/(?P<pk>.*)/$', SymptomGetView.as_view(), name="get_symptom"),

    url(r'^othead/$', OTheadListCreateAPIView.as_view(), name="list_othead"),
    url(r'^validator/$', OTheadDeleteAPIView.as_view(), name="validate_othead"),
#    url(r'^othead/delete/$', OTheadDeleteSOAPConection.as_view(), name="delete_othead"),
    url(r'^mobility/get/othead/(?P<type>.*)/(?P<code>.*)/$', OTheadGetView.as_view(), name="get_head"),    

    url(r'^otoperations/$', OToperationsListCreateAPIView.as_view(), name="list_otoperations"),
    url(r'^otoperations/delete/$', OToperationsDeleteSOAPConection.as_view(), name="delete_otoperations"),
    url(r'^mobility/get/otoperations/(?P<type>.*)/(?P<code>.*)/$', OToperationsGetView.as_view(), name="get_operations"),    

    url(r'^otcomponent/$', OTcomponentListCreateAPIView.as_view(), name="list_otcomponent"),
    url(r'^otcomponent/delete/$', OTcomponentDeleteSOAPConection.as_view(), name="delete_otcomponent"),
    url(r'^mobility/get/otcomponent/(?P<type>.*)/(?P<code>.*)/$', OTcomponentGetView.as_view(), name="get_component"),    

    url(r'^otmeasures/$', OTMeasuresCreate.as_view(), name="list_otmeasures"),
    url(r'^otmeasures/list/$', OTmeasuresListCreateAPIView.as_view(), name="list_otmeasures"),
    url(r'^otmeasures/delete/$', OTmeasuresDeleteSOAPConection.as_view(), name="delete_otmeasures"),
    url(r'^mobility/get/otmeasures/$', OTmeasuresGetView.as_view(), name="list_otmeasures"),
    url(r'^mobility/get/otmeasures/(?P<type>.*)/(?P<code>.*)/$', GetMeasures.as_view(), name = "get_measures"),


    url(r'^ticket/$', TicketListCreateAPIView.as_view(), name="post_ticket"),
    url(r'^get/ticket/$', TicketFilterAPIView.as_view(), name="list_ticket"),
    url(r'^ticket/(?P<pk>[0-9]+)/$', TicketUpdateDetailAPIView.as_view(), name="detail_ticket"),
    url(r'^ticket/admin/$', TicketAdminFilter.as_view(), name = "admin_ticket"),
    url(r'^mobility/set/ticket/$', TicketPostAPIView.as_view(), name="add_ticket"),
    url(r'^mobility/get/ticket/(?P<center>.*)/$', TicketGetAPIView.as_view(), name="add_ticket"),

    url(r'^otnotification/$', OTnotificationListCreateAPIView.as_view(), name="list_otnotification"),
    url(r'^mobility/get/notification/(?P<type>.*)/(?P<code>.*)/$', Notification.as_view(), name="list_otnotification"),


    url(r'^otclosing/$', OTclosingListAPIView.as_view(), name="list_otclosing"),
    url(r'^get/otclosing/$', OTclosingGETWeb.as_view(), name="list_otclosing"),
    url(r'^mobility/set/ot/$', OTclosingGetView.as_view(), name="post_otclosing"),
    url(r'^mobility/set/otupdate/$', OTclosingUpdateAPI.as_view(), name="put_otclosing"),
    url(r'^mobility/set/incidence/$', OTIncidence.as_view(), name="post_incidence"),
 

    url(r'^log/$', LogAPIView.as_view(), name="list_log"),
    url(r'^log/detail/$', LogDetailAPIView.as_view(), name="detail_log"),
    url(r'^log/consult/$', LogWebAPIView.as_view(), name="consult_log"),

    url(r'^data/$', ResultSendWeb.as_view(), name="detail_data"),
    url(r'^get/data/$', SendWebAPIView.as_view(), name="get_data"),
    url(r'^grafic/$', SendGraficAPIWeb.as_view(), name="detail_grafic"),

    url(r'^mobility/admin/group/$', GroupCenterAdmin.as_view(), name="group_admin"),
    url(r'^mobility/admin/filter/$', AdminFilter.as_view(), name="filter_admin"),
    url(r'^mobility/admin/detail/$', DetailFilter.as_view(), name="detail_admin"),

    url(r'^mobility/admin/filter/detail/$', AdminFilterDetail.as_view(), name="admin_filter_detail"),

    url(r'^mobility/set/map/$', MapService.as_view(), name = "map_service"),
    url(r'^mobility/map/(?P<pk>[0-9]+)/$', MapServiceDetail.as_view(), name = "map_detail"),

    url(r'^get/map/$', MapWebService.as_view(), name = "web_map"),

    url(r'^get/report/$', SuccessReport.as_view(), name = "valid_report"),
    url(r'^report/$', ReportOT.as_view(), name = "report"),
    url(r'^get/report/ticket/$', SuccessReportTicket.as_view(), name = "vallid_report_ticket"),
    url(r'^report/ticket/$', ReportTicket.as_view(), name = "report_Ticket"),
    url(r'^get/report/technic/$', SuccessReportTechnic.as_view(), name = "valid_report_Technic"),
    url(r'^report/technic/$', ReportTechnic.as_view(), name = "report_Technic"),

]
