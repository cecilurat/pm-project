#/usr/bin/env python
# -*- coding: utf-8 -*-

from django.shortcuts import render
from django.http import HttpRequest
from django.template import RequestContext
from .models import *
from accounts.models import UserAPP
from zeep import Client, Settings
from requests import Session
import json
import csv
import xlwt
from django.http import HttpResponse
from zeep.transports import Transport
from requests.auth import HTTPBasicAuth
from rest_framework.views import APIView
from django.contrib.auth.decorators import login_required
from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView
from rest_framework.response import Response
from .serializer import *
from rest_framework.permissions import IsAuthenticated
from datetime import datetime, timedelta
from django.utils import timezone


class ServiceAux(APIView):
	def get(self, request, code, date, format=None):
		otoperations = OToperations.objects.filter(OBJID = code, BEGDAOP = date)
		for o in otoperations:
			otoperation = COTOperations.objects.filter(AUFNR = o.AUFNR)
			if len(otoperation) > 0:
				o.DONE = "1"
				o.save()
		return Response({"response":"success"})

#Center
class CenterListCreateAPIView(ListCreateAPIView):
	serializer_class = CenterSerializer
	def get_queryset(self):
		return Center.objects.all()

class CenterListView(APIView):
	def get(self, request, code, format=None):
		if code != "0":
			plans = GroupPlanCenter.objects.filter(INGRP = code).values("IWERK")
			centers = Center.objects.filter(WERKS__in = plans)
		else:
			centers = Center.objects.all()
		serializer = CenterDefineSerializer(centers, many = True)
		return Response(serializer.data)

class CenterDeleteSOAPConection(RetrieveUpdateDestroyAPIView):
	def delete(self, request, format=None):
		centers = Center.objects.all()
		num = len(centers)
		Center.objects.all().delete()
		text = "Se borraron " + str(num)
		return Response(text)

class GeolocationCenterAPI(APIView):
	def post(self, request, format=None):
		werks = request.data.get("pk")
		try:
			center = Center.objects.get(WERKS = werks)
			try:
				geolocation = Geolocation.objects.get(CODE = werks)
				serializer = CenterGeolocationSerializer(center)
				return Response({"response":"success", "data": serializer.data})
			except Geolocation.DoesNotExist:
				serializer = CenterNameSerializer(center)
				return Response({"response":"success", "data": serializer.data})
		except Center.DoesNotExist:
			return Response({"response":"error"})

#OrderClass
class OrderClassListCreateAPIView(ListCreateAPIView):
	serializer_class = OrderClassSerializer
	def get_queryset(self):
		return OrderClass.objects.all()

class OrderClassDeleteSOAPConection(RetrieveUpdateDestroyAPIView):
	def delete(self, request, format=None):
		orders = OrderClass.objects.all()
		num = len(orders)
		OrderClass.objects.all().delete()
		text = "Se borraron " + str(num)
		return Response(text)

#Location
class LocationListCreateAPIView(ListCreateAPIView):
	serializer_class = LocationSerializer
	def get_queryset(self):
		return Location.objects.all()

class LocationDeleteSOAPConection(RetrieveUpdateDestroyAPIView):
	def delete(self, request, format=None):
		locations = Location.objects.all()
		num = len(locations)
		Location.objects.all().delete()
		text = "Se borraron " + str(num)
		return Response(text)

class LocationGetView(APIView):
	def get(self, request, pk, format=None):
		locations = Location.objects.filter(SWERK = pk)
		serializer = LocationSerializer(locations, many=True)
		return Response({'response': 'success', 'data': serializer.data})


#Equipment
class EquipmentListCreateAPIView(ListCreateAPIView):
	serializer_class = EquipmentSerializer
	def get_queryset(self):
		return Equipment.objects.all()

class EquipmentDeleteSOAPConection(RetrieveUpdateDestroyAPIView):
	def delete(self, request, format=None):
		equipments = Equipment.objects.all()
		num = len(equipments)
		Equipment.objects.all().delete()
		text = "Se borraron " + str(num)
		return Response(text)

class EquipmentGetView(APIView):
	def get(self, request, pk, format=None):
		if pk == "None":
			content = {'response':'error'}
			return Response(content)
		else:
			equipments = Equipment.objects.filter(SWERK = pk)
		serializer = EquipmentSerializer(equipments, many=True)
		return Response({'response': 'success', 'data': serializer.data})

#Planning
class PlanningListCreateAPIView(ListCreateAPIView):
	serializer_class = PlanningSerializer
	def get_queryset(self):
		return Planning.objects.all()

class PlanningDeleteSOAPConection(RetrieveUpdateDestroyAPIView):
	def delete(self, request, format=None):
		plannings = Planning.objects.all()
		num = len(plannings)
		Planning.objects.all().delete()
		text = "Se borraron " + str(num)
		return Response(text)


#Priority
class PriorityListCreateAPIView(ListCreateAPIView):
	serializer_class = PrioritySerializer
	def get_queryset(self):
		return Priority.objects.all()

class PriorityDeleteSOAPConection(RetrieveUpdateDestroyAPIView):
	def delete(self, request, format=None):
		priorities = Priority.objects.all()
		num = len(priorities)
		Priority.objects.all().delete()
		text = "Se borraron " + str(num)
		return Response(text)


#GroupPlanCenter
class GroupPlanCenterListCreateAPIView(ListCreateAPIView):
	serializer_class = GroupPlanCenterSerializer
	def get_queryset(self):
		return GroupPlanCenter.objects.all()

class GroupPlanCenterDeleteSOAPConection(RetrieveUpdateDestroyAPIView):
	def delete(self, request, format=None):
		groups = GroupPlanCenter.objects.all()
		num = len(groups)
		GroupPlanCenter.objects.all().delete()
		text = "Se borraron " + str(num)
		return Response(text)


#JobPosition
class JobPositionListCreateAPIView(ListCreateAPIView):
	serializer_class = JobPositionSerializer
	def get_queryset(self):
		return JobPosition.objects.all()

class JobPositionDeleteSOAPConection(RetrieveUpdateDestroyAPIView):
	def delete(self, request, format=None):
		jobs = JobPosition.objects.all()
		num = len(jobs)
		JobPosition.objects.all().delete()
		text = "Se borraron " + str(num)
		return Response(text)


#Measurement
class MeasurementListCreateAPIView(ListCreateAPIView):
	serializer_class = MeasurementSerializer
	def get_queryset(self):
		return Measurement.objects.all()

class MeasurementDeleteSOAPConection(RetrieveUpdateDestroyAPIView):
	def delete(self, request, format=None):
		measurements = Measurement.objects.all()
		num = len(measurements)
		Measurement.objects.all().delete()
		text = "Se borraron " + str(num)
		return Response(text)


#Symptom
class SymptomListCreateAPIView(ListCreateAPIView):
	serializer_class = SymptomSerializer
	def get_queryset(self):
		return Symptom.objects.all()

class SymptomDeleteSOAPConection(RetrieveUpdateDestroyAPIView):
	def delete(self, request, format=None):
		symptoms = Symptom.objects.all()
		num = len(symptoms)
		Symptom.objects.all().delete()
		text = "Se borraron " + str(num)
		return Response(text)

class SymptomGetView(APIView):
	def get(self, request, pk, format=None):
		equipments = Equipment.objects.filter(SWERK = pk)
		symptoms = []
		for e in equipments:
			if e.RBNR == "None" or  e.RBNR == "":
				pass
			else:
				symps = Symptom.objects.filter(RBNR = e.RBNR)
				for s in symps:
					symptoms.append(s)
		serializer = SymptomSerializer(symptoms, many=True)
		return Response({'response': 'success', 'data': serializer.data})


#NoticeType
class NoticeTypeListCreateAPIView(ListCreateAPIView):
	serializer_class = NoticeTypeSerializer
	def get_queryset(self):
		return NoticeType.objects.all()

class NoticeTypeUpdateDetailAPIView(RetrieveUpdateDestroyAPIView):
	serializer_class = NoticeTypeSerializer
	queryset = NoticeType.objects.all()

class NoticeTypeDeleteSOAPConection(RetrieveUpdateDestroyAPIView):
	def delete(self, request, format=None):
		notices = NoticeType.objects.all()
		num = len(notices)
		NoticeType.objects.all().delete()
		text = "Se borraron " + str(num)
		return Response(text)

class NoticeTypeListView(APIView):
	def get(self, format=None):
		notices = NoticeType.objects.all()
		serializer = NoticeTypeSerializer(notices, many=True)
		return Response({'response': 'success', 'data': serializer.data})

#ServiceType
class ServiceTypeListCreateAPIView(ListCreateAPIView):
	serializer_class = ServiceTypeSerializer
	def get_queryset(self):
		return ServiceType.objects.all()

class ServiceTypeDeleteSOAPConection(RetrieveUpdateDestroyAPIView):
	def delete(self, request, format=None):
		services = ServiceType.objects.all()
		num = len(services)
		ServiceType.objects.all().delete()
		text = "Se borraron " + str(num)
		return Response(text)


#Geolocation
class GeolocationListCreateAPIView(ListCreateAPIView):
	serializer_class = GeolocationSerializer
	def get_queryset(self):
		return Geolocation.objects.all()

class GeolocationUpdateDetailAPIView(RetrieveUpdateDestroyAPIView):
	serializer_class = GeolocationSerializer
	queryset = Geolocation.objects.all()

class GeolocationDeleteSOAPConection(RetrieveUpdateDestroyAPIView):
	def delete(self, request, format=None):
		geolocations = Geolocation.objects.all()
		num = len(geolocations)
		Geolocation.objects.all().delete()
		text = "Se borraron " + str(num)
		return Response(text)


#OThead
class OTheadListCreateAPIView(ListCreateAPIView):
	serializer_class = OTheadSerializer
	def get_queryset(self):
		return OThead.objects.all()

class OTheadDeleteAPIView(ListCreateAPIView):
	def post(self, request, format=None):
		aufnr = request.data.get("AUFNR")
		head = request.data.get("HEAD")
		if head == "1":
			try:
				othead = OThead.objects.get(AUFNR = aufnr)
				try:
					otclos = OTclosing.objects.get(AUFNR = othead.AUFNR, OBJID = athead.OBJID)
					return Response({'response': '1'})
				except:
					try:
						measures = OTmeasures.objects.filter(AUFNR = aufnr).delete()
					except:
						pass
					try:
						components = OTcomponent.objects.filter(AUFNR = aufnr).delete()
					except:
						pass
					try:
						operations = OToperations.objects.filter(AUFNR = aufnr).delete()
					except:
						pass
					othead.delete()
					return Response({'response': '0'})
			except:
				return  Response({'response': '0'})
		else:
			try:
				othead = OThead.objects.get(AUFNR = aufnr)
				try:
					otclos = OTclosing.objects.get(AUFNR = othead.AUFNR, OBJID = othead.OBJID)
					return Response({'response': '1'})
				except:
					return Response({'response': '0'})
			except:
				return Response({'response': '1'})

class OTheadGetView(APIView):
	def get(self, request, type, code, format=None):
		if type == 'T':
			otoperations = OToperations.objects.filter(OBJID = code).exclude(DONE = "1").values_list('OBJID','AUFNR').distinct()
			if not otoperations:
				return Response({'response': 'success', 'data': []})
		else:
			otoperations = OToperations.objects.filter(SOWRK = code).values_list('OBJID','AUFNR').distinct()
			if not otoperations:
				return Response({'response': 'sucess', 'data': []})
		dateini = datetime.now() - timedelta(6 * 365/12)
		datefin = datetime.now()
		otclos = OTclosing.objects.all().values("AUFNR")
		otheads = OThead.objects.filter(AUFNR__in = otoperations.values("AUFNR"), CREATED__range = [dateini, datefin]).exclude(NOTNR = "ESRP").exclude(NOTNR = "COMP").exclude(NOTNR = "ESSE").exclude(AUFNR__in = otclos)
		serializer = OTheadMobileSerializer(otheads, many = True)
		return Response(serializer.data)

#OToperations
class OToperationsListCreateAPIView(ListCreateAPIView):
	serializer_class = OToperationsSerializer
	def get_queryset(self):
		return OToperations.objects.all()
	def post(self, request, format=None):
		serializer = OToperationsSerializer(data=request.data)
		if serializer.is_valid():
			otoperations = serializer.save()
			otoperations.SOWRK = OThead.objects.get(AUFNR = otoperations.AUFNR).SOWRK
			otoperations.save()
			return Response(serializer.data)
		return Response(serializer.errors)

class OToperationsUpdateDetailAPIView(RetrieveUpdateDestroyAPIView):
	serializer_class = OToperationsSerializer
	queryset = OToperations.objects.all()

class OToperationsDeleteSOAPConection(RetrieveUpdateDestroyAPIView):
	def delete(self, request, format=None):
		otoperations = OToperations.objects.all()
		num = len(otoperations)
		OToperations.objects.all().delete()
		text = "Se borraron " + str(num)
		return Response(text)



#########From mobile
class OToperationsListView(APIView):
	def post(self, format=None):
		otoperations = OToperations.objects.all()
		serializer = OToperationsSerializer(otoperations, many=True)
		return Response({'response': 'success', 'data': serializer.data})

class OToperationsCreateView(APIView):
	def post(self, request, format=None):
		serializer = OToperationsSerializer(data=request.data)
		if serializer.is_valid():
			serializer.save()
			return Response({'response': 'success'})
		else:
			return Response({'response': 'error'})

class OToperationsUpdateView(APIView):
	def post(self, request, pk, format=None):
		otoperations = OToperations.objects.get(pk=pk)
		serializer = OToperationsSerializer(otoperations, data=request.data)
		if serializer.is_valid():
			serializer.save()
			return Response({'response': 'success'})
		else:
			return Response({'response': 'error'})

class OToperationsDeleteView(APIView):
	def post(self, request, pk, format=None):
		otoperations = OToperations.objects.get(pk=pk)
		if otoperations is not None:
			otoperations.delete()
			return Response({'response': 'success'})
		else:
			return Response({'response': 'error'})

class OToperationsGetView(APIView):
	def get(self, request, type, code, format=None):
		otoperations = []
		if type == 'T':
			ots = OToperations.objects.filter(OBJID = code).exclude(DONE = "1").values("AUFNR")
			otclos = OTclosing.objects.all().values("AUFNR")
			otheads = OThead.objects.filter(AUFNR__in = ots).exclude(AUFNR__in = otclos).exclude(NOTNR = "ESRP").exclude(NOTNR = "COMP").exclude(NOTNR = "ESSE").values("AUFNR")
			otoperations = OToperations.objects.filter(AUFNR__in = otheads)
		else:
			ots = OToperations.objects.filter(SOWRK = code).values("AUFNR")
			otclos = OTclosing.objects.get(OBJID = code, AUFNR__in = ots, TECHAPP = "1", CUSAPP = "0").values("AUFNR")
			otoperations = ots.filter(AUFNR__in = otclos)		
		serializer = OToperationsSerializer(otoperations, many=True)
		return Response({'response': 'success', 'data': serializer.data})


#OTcomponent
class OTcomponentListCreateAPIView(ListCreateAPIView):
	serializer_class = OTcomponentSerializer
	def get_queryset(self):
		return OTcomponent.objects.all()
	def post(self, request, format=None):
		serializer = OTcomponentSerializer(data=request.data)
		if serializer.is_valid():
			otcomponent = serializer.save()
			otcomponent.SOWRK = OThead.objects.get(AUFNR = otcomponent.AUFNR).SOWRK
			try:
				coto = COTcomponent.objects.get(AUFNR = otcomponent.AUFNR, OBJID = otcomponent.OBJID)
				otcomponent.DONE = coto.DONE
				otcomponent.OPOBS = coto.OPOBS
				otcomponent.OPTIME = coto.OPTIME
			except:
				otcomponent.DONE = ""
				otcomponent.OPOBS = ""
				otcomponent.OPTIME = ""
			otcomponent.save()
			return Response(serializer.data)
		return Response(serializer.errors)

class OTcomponentUpdateDetailAPIView(RetrieveUpdateDestroyAPIView):
	serializer_class = OTcomponentSerializer
	queryset = OTcomponent.objects.all()

class OTcomponentDeleteSOAPConection(RetrieveUpdateDestroyAPIView):
	def delete(self, request, format=None):
		otcomponent = OTcomponent.objects.all()
		num = len(otcomponent)
		OTcomponent.objects.all().delete()
		text = "Se borraron " + str(num)
		return Response(text)

class OTcomponentGetView(APIView):
	def get(self, request, type, code, format=None):
		if type == 'T':
			comps = OTcomponent.objects.filter(OBJID = code).values("AUFNR")
			otclos = OTclosing.objects.all().values("AUFNR")
			otheads = OThead.objects.filter(AUFNR__in = comps).exclude(AUFNR__in = otclos).exclude(NOTNR = "ESRP").exclude(NOTNR = "COMP").exclude(NOTNR = "ESSE").values("AUFNR")
			otcomponents = OTcomponent.objects.filter(AUFNR__in = otheads)
		else:
			otclos = OTclosing.objects.filter(TECHAPP = "1", CUSAPP = "0").values("AUFNR")
			ots = OThead.objects.filter(SOWRK = code, AUFNR__in = otclos).values("AUFNR")
			otcomponents = OTcomponent.objects.filter(AUFNR__in  = ots)
		serializer = OTcomponentSerializer(otcomponents, many=True)
		return Response({'response': 'success', 'data': serializer.data})

#########From mobile
class OTcomponentListView(APIView):
	def post(self, format=None):
		otcomponent = OTcomponent.objects.all()
		serializer = OTcomponentSerializer(otcomponent, many=True)
		return Response({'response': 'success', 'data': serializer.data})

class OTcomponentCreateView(APIView):
	def post(self, request, format=None):
		serializer = OTcomponentSerializer(data=request.data)
		if serializer.is_valid():
			serializer.save()
			return Response({'response': 'success'})
		else:
			return Response({'response': 'error'})

class OTcomponentUpdateView(APIView):
	def post(self, request, pk, format=None):
		otcomponent = OTcomponent.objects.get(pk=pk)
		serializer = OTcomponentSerializer(otcomponent, data=request.data)
		if serializer.is_valid():
			serializer.save()
			return Response({'response': 'success'})
		else:
			return Response({'response': 'error'})

class OTcomponentDeleteView(APIView):
	def post(self, request, pk, format=None):
		otcomponent = OTcomponent.objects.get(pk=pk)
		if otcomponent is not None:
			otcomponent.delete()
			return Response({'response': 'success'})
		else:
			return Response({'response': 'error'})

#OTmeasures
class OTmeasuresListCreateAPIView(ListCreateAPIView):
	serializer_class = OTmeasuresSerializer
	def get_queryset(self):
		return OTmeasures.objects.all()

class OTMeasuresCreate(APIView):
	def post(self, request, format=None):
		aufnr = request.data.get("AUFNR")
		point = request.data.get("POINT")
		equnr = request.data.get("EQUNR")
		value = request.data.get("VALUE")
		date = request.data.get("DATE")
		time = request.data.get("TIME")
		othead = OThead.objects.filter(AUFNR = aufnr).first()
		otmeasures = OTmeasures(AUFNR = aufnr,
				POINT = point,
				EQUNR = equnr,
				VALUE = value,
				DATE = date,
				TIME = time,
				OBJID = othead.OBJID)
		otmeasures.save()
		return Response({'response': '0'})


class OTmeasuresUpdateDetailAPIView(RetrieveUpdateDestroyAPIView):
	serializer_class = OTmeasuresSerializer
	queryset = OTmeasures.objects.all()

class OTmeasuresDeleteSOAPConection(RetrieveUpdateDestroyAPIView):
	def delete(self, request, format=None):
		otmeasures = OTmeasures.objects.all()
		num = len(otmeasures)
		OTmeasures.objects.all().delete()
		text = "Se borraron " + str(num)
		return Response(text)

#########From mobile
class OTmeasuresListView(APIView):
	def get(self, format=None):
		otmeasures = OTmeasures.objects.all()
		serializer = OTmeasuresSerializer(otmeasures, many=True)
		return Response({'response': 'success', 'data': serializer.data})

class OTmeasuresCreateView(APIView):
	def post(self, request, format=None):
		serializer = OTmeasuresSerializer(data=request.data)
		if serializer.is_valid():
			serializer.save()
			return Response({'response': 'success'})
		else:
			return Response({'response': 'error'})

class OTmeasuresUpdateView(APIView):
	def post(self, request, pk, format=None):
		otmeasures = OTmeasures.objects.get(pk=pk)
		serializer = OTmeasuresSerializer(otmeasures, data=request.data)
		if serializer.is_valid():
			serializer.save()
			return Response({'response': 'success'})
		else:
			return Response({'response': 'error'})

class OTmeasuresDeleteView(APIView):
	def post(self, request, pk, format=None):
		otmeasures = OTmeasures.objects.get(pk=pk)
		if otmeasures is not None:
			otmeasures.delete()
			content = {'response': 'success'}
			return Response(content)
		else:
			content = {'response': 'error'}
			return Response(content)


class OTmeasuresGetView(APIView):
	def get(self, request, format=None):
		otmeasures = OTmeasures.objects.all()
		serializer = OTmeasuresMobileSerializer(otmeasures, many = True)
		return Response({"response": "success", "data": serializer.data})

class GetMeasures(APIView):
	def get(self, request, type, code, format=None):
		if type == "T":
			meas = OTmeasures.objects.filter(OBJID = code).values("AUFNR")
			otclos = OTclosing.objects.filter(TECHAPP = 0, CUSAPP = 0, OBJID = code).values("AUFNR")
			otheads = OThead.objects.filter(AUFNR__in = meas).exclude(AUFNR__in = otclos).exclude(NOTNR = "ESRP").exclude(NOTNR = "COMP").exclude(NOTNR = "ESSE").values("AUFNR")
			otmeasures = OTmeasures.objects.filter(AUFNR__in = otheads)
		else:
			otheads = OThead.objects.filter(SOWRK = code).values("AUFNR")
			otclos  = OTclosing.objects.filter(TECHAPP = 1, CUSAPP = 0, AUFNR__in = otheads, OBJID =o.OBJID).values("AUFNR")
			otmeasures = OTmeasures.objects.filter(AUFNR__in = otclos)
		serializer = OTmeasuresMobileSerializer(otmeasures, many = True)
		return Response({'response': 'success', 'data': serializer.data})


#=====================SALIDA
#Ticket
class TicketListCreateAPIView(APIView):
	def post(self, request, format=None):
		_ticket = request.data.get("data")
		ticket_list = _ticket.split(",")
		tickets = []
		fly = "0"
		for i in ticket_list:
			ticket = Ticket.objects.get(id = i)
			tickets.append(ticket)
			session = Session()
			session.auth = HTTPBasicAuth("wsintur", "inturws2019")
			client = Client('http://172.29.0.206:50000/dir/wsdl?p=ic/9016de8e7d8931a3b8f5541ba112278e', transport=Transport(session=session))
			try:
				result = client.service.SI_TicketSoporte_Req(REQUEST= {'ITEM' : {'TICNR' : ticket.id, 'QMART' : ticket.QMART, 'WERKS' : ticket.WERKS, 'TPLNR' : ticket.TPLNR, 'EQUNR': ticket.EQUNR, 'NDATE': ticket.NDATE, 'NTIME': ticket.NTIME, 'CODEGRUPPE': ticket.CODEGRUPPE, 'CODE': ticket.CODE, 'REASON': ticket.REASON, 'STOPPED': ticket.STOPPED}})
				res = result[0]
				ticket.STATUS = "1"
				ticket.SAP = " "
				for r in result:
					if r["MSGTYPE"] == "E":
						ticket.STATUS = "0"
					else:
						ticket.NUDOC =  r["MSGDCNR"]
					ticket.SAP = ticket.SAP + "_" + str(r["MSGTYPE"])+" | "+str(r["TICNR"])+" | "+str(r["MSGDCNR"])+" | "+str(r["MSGDETAIL"])
					ticket.save()
				if fly == "0":
					content = {'response': '1'}
			except:
				ticket.STATUS = "2"
				ticket.NUDOC = ""
				ticket.save()
				content = {'response': '0'}
				fly = "1"
			return Response(content)

class TicketUpdateDetailAPIView(RetrieveUpdateDestroyAPIView):
	serializer_class = TicketSerializer
	queryset = Ticket.objects.all()


class TicketFilterAPIView(APIView):
	def post(self, request, format=None):
		center = request.data.get("center")
		status = request.data.get("status")
		sap = request.data.get("sap")
		plan = request.data.get("plan")
		dti = datetime.strptime(request.data.get("dateini"), "%Y-%m-%d")
		dtf = datetime.strptime(request.data.get("datefin"), "%Y-%m-%d")
		tickets = Ticket.objects.filter(NDATE__range = [dti, dtf])
		if plan != "0":
			groups = GroupPlanCenter.objects.filter(INGRP = plan).values("IWERK")
			tickets = tickets.filter(WERKS__in = groups)
		else:
			if center != "0":
				tickets = tickets.filter(WERKS = center)
		if status != "3":
			tickets = tickets.filter(APPROVAL = status)
		if sap != "3":
			tickets = tickets.filter(STATUS = sap)
		serializer = TicketSerializer(tickets, many=True)
		content = {'response': 'success', 'data': serializer.data}
		return Response(content)

class TicketPostAPIView(APIView):
	def post(self, request, format=None):
		data = request.data.get("data")
		fly = "0"
		ticket = json.loads(data)
		tickets = Ticket.objects.filter(EQUNR = ticket["EQUNR"], APPROVAL = "0")
		if len(tickets) > 0:
			content = {'data':'error', 'response':'	No es posible crear nuevo Ticket debido a que ya se registro el Ticket '+str(tickets[0].id)+' para el equipo reportado.'}
			return Response(content)
		else:
			orders = OThead.objects.filter(EQUNR = ticket["EQUNR"])
			for o in orders:
				close = OTclosing.objects.filter(AUFNR = o.AUFNR)
				if len(close) ==  0 and o.AUART != "ZPM2":
					content = {'data': 'error', 'response': 'No es posible crear nuevo Ticket debido a que ya existe una Orden de Trabajo '+ o.AUFNR + ' en proceso para el equipo reportado.'}
					return Response(content)
		serializer = TicketSerializer(data=ticket)
		if serializer.is_valid():
			ticket = serializer.save()
			center = Center.objects.get(WERKS = ticket.WERKS)
			ticket.NWERKS = center.NAME1
			notice = NoticeType.objects.get(CODE = ticket.QMART)
			ticket.NQMART = notice.DESCRIPTION
			ticket.STATUS = "0"
			ticket.save()
			content = {'response': 'success'}
			return Response(content)
		else:
			print("error")
			content = {'response': 'Error', 'data': 'Datos erroeneos'}
			return Response(content)

class TicketGetAPIView(APIView):
	def get(self, request, center ,format=None):
		tickets = Ticket.objects.filter(WERKS = center)
		ticket = []
		dt = str(timezone.now())
		__date = dt.split('T')
		_date = __date[0].split(' ')
		date = datetime.strptime(_date[0], '%Y-%m-%d')
		date2 = date - timedelta(days = 7)
		for t in tickets:
			dt = str(t.CREATED)
			_dat = dt.split('T')
			dat = _dat[0].split(' ')
			dato = datetime.strptime(dat[0], '%Y-%m-%d')
			if dato > date2:
				ticket.append(t)
		serializer = TicketSerializer(ticket, many=True)
		content = {'data': serializer.data}
		return Response(content)


class TicketUpdateView(APIView):
	def post(self, request, pk, format=None):
		tickets = Ticket.objects.get(pk=pk)
		serializer = TicketSerializer(tickets, data=request.data)
		if serializer.is_valid():
			serializer.save()
			content = {'response': 'success'}
			return Response(content)
		else:
			content = {'response': 'error'}
			return Response(content)


class TicketDeleteView(APIView):
	def post(self, request, pk, format=None):
		tickets = Ticket.objects.get(pk=pk)
		if tickets is not None:
			tickets.delete()
			content = {'response': 'success'}
			return Response(content)
		else:
			content = {'response': 'error'}
			return Response(content)

class TicketAdminFilter(APIView):
	def post(self, request, format=None):
		dateini = datetime.strptime(request.data.get("DATINI"), '%Y-%m-%d')
		datefin = datetime.strptime(request.data.get("DATFIN"), '%Y-%m-%d')
		center = request.data.get("CENTER")
		status = request.data.get("STATUS")
		tickets = Ticket.objects.filter(NDATE__range = [dateini, datefin])
		if center != "0":
			tickets = tickets.filter(WERKS__in = center.split(","))
		if status != "3":
 			tickets = tickets.filter(APPROVAL = status)
 		serializer = TicketSerializer(tickets, many = True)
		return Response({'response': 'success', 'data': serializer.data})

#OTnotification
class OTnotificationListCreateAPIView(ListCreateAPIView):
	serializer_class = OTnotificationSerializer
	def get_queryset(self):
		return OTnotification.objects.all()
	def post(self, request, format=None):
		serializer = OTnotificationSerializer(data=request.data)
		if serializer.is_valid():
			OTnotification = serializer.save()
			dt = str(datetime.now())
			_date = dt.split('T')
			date = _date[0].split(' ')
			OTnotification.ADATE = date[0]
			OTnotification.ATIME = date[1].split('.')[0]
			OTnotification.save()
			session = Session()
			session.auth = HTTPBasicAuth("wsintur", "inturws2019")
			client = Client('http://172.29.0.206:50000/dir/wsdl?p=ic/43a4fad9ba0030969d123c577ed1d8f7', transport=Transport(session=session))
			result = client.service.SI_NotificacionOT_Req(REQUEST= {'ITEM' : {'AUFNR' : OTnotification.AUFNR, 'DATE' : OTnotification.ADATE, 'TIME' : OTnotification.ATIME}})
			return Response(serializer.data)
		return Response(serializer.errors)

class OTnotificationUpdateDetailAPIView(RetrieveUpdateDestroyAPIView):
	serializer_class = OTnotificationSerializer
	queryset = OTnotification.objects.all()

class OTclosingListAPIView(APIView):
	def post(self, request, format=None):
		otclos = []
		send = ''
		countClos = 0
		center = request.data.get("center")
		code = request.data.get("code")
		plan = request.data.get("plan")
		approval = request.data.get("approval")
		sap = request.data.get("sap")
		dti = datetime.strptime(request.data.get("dateini"), "%Y-%m-%d")
		dtf = datetime.strptime(request.data.get("datefin"), "%Y-%m-%d")
		if dti == dtf:
			date = request.data.get("dateini")
			otclosings = OTclosing.objects.filter(ENDDA = date)
		else:
			otclosings = []
			days = dtf -dti
			days = int(str(days)[:2])
			for i in range(days + 1):
				date = str(dti + timedelta(days = i))[:10]
				otclos = OTclosing.objects.filter(ENDDA = date)
				for ot in otclos:
					otclosings.append(ot)
		otclos = []
		for ot in otclosings:
			othead = OThead.objects.get(AUFNR = ot.AUFNR)
			if sap == "3" and approval == "2" and center == "0" and plan == "0" and code == "0":
				cent = "1"
				jobpos = "1"
				group = "1"
				cusapp = "1"
				status = "1"
			else:
				if sap != "3" and sap == ot.STATUS:
					status = "1"
				else:
					if sap == "3":
						status = "1"
					else:
						status = "0"
				if approval == "2":
					cusapp = "1"
				else:
					if approval == ot.CUSAPP:
						cusapp = "1"
					else:
						cusapp = "0"
				if center != "0" and center == othead.SOWRK:
					cent = "1"
				else:
					if center == "0":
						cent = "1"
					else:
						cent = "0"
				if plan != "0" and othead.INGRP == plan:
					group = "1"
				else:
					if plan == "0":
						group = "1"
					else:
						group = "0"

				if code != "0" and ot.OBJID == code:
					jobpos = "1"
				else:
					if code =="0":
						jobpos = "1"
					else:
						jobpos = "0"

			if cent == "1" and jobpos == "1" and group == "1" and cusapp == "1" and status == "1":
				if countClos > 0:
					send = send + ','
				pln = othead.INGRP + " " + Planning.objects.get(INGRP = othead.INGRP).INNAM
				ctn = Center.objects.get(WERKS = othead.SOWRK).NAME1
				ubi = othead.TPLNR + " " + Center.objects.get(WERKS = othead.IWERK).NAME1
				try:
					equr = othead.EQUNR + " "+  Equipment.objects.filter(EQUNR = othead.EQUNR).first().EQKTX
				except:
					equr = " "
				try:
					job = JobPosition.objects.get(OBJID = ot.OBJID)
					ARBPL = job.ARBPL
					NARBPL = job.KTEXT
					WERKS = job.WERKS
				except:
					ARBPL = ""
					NARBPL = ""
					WERKS = ""
				if ot.BEGTI == "":
					ot.BEGTI = ot.ENDTI
				if ot.BEGDA == "":
					ot.BEGDA = ot.ENDDA
				co = []
				cotoperations = COTOperations.objects.filter(AUFNR = ot.AUFNR, OBJID = ot.OBJID)
				for coto in cotoperations:
					o = COToperationsSerializer(data = {'id': str(coto.id), 'AUFNR':coto.AUFNR, 'VORNR': coto.VORNR, 'LTXA1': coto.LTXA1, 'DONE': coto.DONE, 'OBJID': coto.OBJID, 'OPOBS': coto.OPOBS, 'OPTIME': coto.OPTIME})
					o.is_valid()
					co.append(o.data)
				cc = []
				cotcomponent = COTcomponent.objects.filter(AUFNR = ot.AUFNR, OBJID = ot.OBJID)
				for coto in cotcomponent:
					c = COTcomponentSerializer(data = {'id':str(coto.id), 'AUFNR':coto.AUFNR, 'VORNR': coto.VORNR, 'MAKTX': coto.MAKTX, 'MATNR': coto.MATNR, 'MEINS':coto.MEINS, 'MENGE': coto.MENGE, 'POSNR': coto.POSNR, 'OBJID': coto.OBJID, 'DONE': coto.DONE, 'CPOBS': coto.CPOBS, 'QUANTITY': coto.QUANTITY})				
					c.is_valid()
					cc.append(c.data)
				om = []
				cotmeasures = COTmeasures.objects.filter(AUFNR = ot.AUFNR, OBJID = ot.OBJID)
				for coto in cotmeasures:
					m = COTmeasuresSerializer(data={'id':str(coto.id), 'AUFNR':coto.AUFNR, 'POINT': coto.POINT, 'OBJID': coto.POINT, 'EQUNR': coto.EQUNR, 'NEQUNR': coto.NEQUNR, 'TAKEN': coto.TAKEN, 'VALUE': coto.VALUE, 'VALUE_DATE': coto.VALUE_DATE, 'VALUE_TIME': coto.VALUE_TIME, 'PMOBS': coto.PMOBS})
					m.is_valid()
					om.append(m.data)
				oto = OTSerializer(data ={'id': str(ot.id), 'AUFNR': ot.AUFNR, 'OBJID': ot.OBJID, 'TECHAPP': ot.TECHAPP, 'CUSAPP': ot.CUSAPP, 'OBSH': ot.OBSH, 'OBSUP': ot.OBSUP, 'BEGDA': ot.BEGDA, 'BEGTI': ot.BEGTI, 'ENDDA': ot.ENDDA, 'ENDTI': ot.ENDTI, 'TOTALHRS': ot.TOTALHRS, 'STATUS': ot.STATUS, 'ADVANCE': ot.ADVANCE, 'SAP': ot.SAP, 'ARBPL': ARBPL, 'NARBPL': NARBPL, 'TITLE': othead.KTEXT, 'PLAN': pln, 'CENTER': ctn, 'LOC':ubi, 'EQUIPO': equr, 'RESPSUP': ot.RESPSUP, 'RESPTEC': ot.RESPTEC, 'VLSUP': ot.VALSUP, 'VALTEC': ot.VALTEC, 'DATESUP': ot.DATESUP, 'TIMESUP': ot.TIMESUP, 'TECHNIC': ARBPL + NARBPL, 'UTECH': ARBPL + WERKS, 'USUP': othead.SOWRK, 'OBSI': othead.OBSI, 'COTOPERATIONS': co, 'COTCOMPONENTS': cc, 'COTMEASURES': om})
				oto.is_valid()
				otclos.append(oto.data)
		return Response({'response': 'success', 'data': otclos})


class OTclosingGETWeb(APIView):
	def post(self, request, format=None):
		_lists = request.data.get("otclosings")
		lists = _lists.split(',')
		fly = "0"
		for _lis in lists:
			lis = _lis.split('-')
			otclos = OTclosing.objects.filter(AUFNR = lis[0], OBJID = lis[1]).first()
			jsonClos = OTclosingSAPSerializer(otclos)

			#from components
			otcom = COTcomponent.objects.filter(AUFNR = lis[0], OBJID = lis[1])
			jsonCom = COTcomponentSAPSerializer(otcom, many = True)

			#from operations
			otoper = COTOperations.objects.filter(AUFNR = lis[0], OBJID = lis[1])
			jsonOpe = COTOperationsSAPSerializer(otoper, many = True)

			#from operations
			otmea = COTmeasures.objects.filter(AUFNR = lis[0], OBJID = lis[1])
			jsonMea = COTmeasureSAPSerializer(otmea, many = True)

			session = Session()
			session.auth = HTTPBasicAuth("wsintur", "inturws2019")
			client = Client('http://172.29.0.206:50000/dir/wsdl?p=ic/ff80244e905e3bfdab94a2f71c8be5cd', transport=Transport(session=session))
			try:
				result = client.service.SI_CerrarOT_Req(REQUEST= {'CABECERA' : jsonClos.data, 'OPERACIONES': jsonOpe.data, 'COMPONENTES': jsonCom.data, 'PMEDIDAS': jsonMea.data})
				res = result[0]
				otclos.STATUS = "1"
				otclos.SAP = " "
				for r in result:
					if r["MSGDCNR3"] is None:
						msg3 = " "
					else:
						msg3 = r["MSGDCNR3"]
					otclos.SAP = otclos.SAP + "_" + r["MSGTYPE"]+ " | "+ r["MSGDCNR1"]+" | "+r["MSGDCNR2"]+" | "+msg3+" | "+r["MSGDETAIL"]
					otclos.save()
					if r["MSGTYPE"] == "E":
						otclos.STATUS = "2"
				if fly == "0":
					content = {'response':'1'}
			except:
				otclos.STATUS = "2"
				otclos.save()
				content = {'response': '0'}
				fly = "1"
			otclos.save()
		return Response(content)

class OTclosingListCreateAPIView(ListCreateAPIView):
	serializer_class = OTclosingSerializer
	def get_queryset(self):
		return OTclosing.objects.all()
	def post(self, request, format=None):
		serializer = OTclosingSerializer(data=request.data)
		if serializer.is_valid():
			OTclosing = serializer.save()
			#	client = Client('http://172.29.0.203:50000/dir/wsdl?p=ic/ff80244e905e3bfdab94a2f71c8be5cd')
			result = client.service.SI_CerrarOT_Req(OTclosing)
			return Response(serializer.data)
		return Response(serializer.errors)

class OTclosingGetView(APIView):
	def post(self, request, format=None):
		data = request.data.get("data")
		cjson = json.loads(data)
		print(str(cjson))
		if cjson["OToperation"] == []:
			print("No hay tareas")
			content = {'response': 'error'}
			return Response(content)
		try:
			resptec = cjson["RESPTEC"]
			valtec = cjson["VALTEC"]
		except:
			resptec = " "
			valtec = " "
		print(cjson["OBSUP"])
		print(cjson["OBSH"])
		otclosing = OTclosing(AUFNR = cjson["AUFNR"],
								OBJID = cjson["OBJID"],
								TECHAPP = cjson["TECHAPP"],
								CUSAPP = cjson["CUSAPP"],
								OBSH = cjson["OBSH"],
								BEGDA = cjson["BEGDA"],
								OBSUP = cjson["OBSUP"],
								BEGTI = cjson["BEGTI"],
								ENDDA = cjson["ENDDA"],
								ENDTI = cjson["ENDTI"],
								ADVANCE = cjson["ADVANCE"],
								TOTALHRS = cjson["TOTALHRS"],
								SAP = "",
								RESPTEC = resptec,
								VALTEC = valtec)
		for otc in cjson["OTcomponent"]:
			serializer = COTcomponenSerializer(data=otc)
			if serializer.is_valid():
				otc = serializer.save()
				content = {'response': 'success'}
			else:
				content = {'response': 'error'}
				return Response(content)
		for oto in cjson["OToperation"]:
			serializer = COToperationSerializer(data=oto)
			if serializer.is_valid():
				serializer.save()
				duplicates = OToperations.objects.filter(AUFNR = oto["AUFNR"], OBJID = oto["OBJID"])
				for d in duplicates:
					d.DONE = "1"
					d.save()
				content = {'response': 'success'}
			else:
				content = {'response': 'error'}
				return Response(content)
		for otm in cjson["OTmeasure"]:
			serializer = COTmeasureSerializer(data = otm)
			if serializer.is_valid():
				serializer.save()
				content = {'response': 'success'}
			else:
				content = {'response': 'error'}
				return Response(content)
		otclosing.TECHAPP = "1"
		otclosing.save()
		print(otclosing.AUFNR)
		content = {'response': 'success'}
		return Response(content)


class OTclosingUpdateAPI(APIView):
	def post(self, request, format=None):
		_data = request.data.get("data")
		data = json.loads(_data)
		try:
			respsup = data["RESPSUP"]
			valsup = data["VALSUP"]
		except:
			respsup = " "
			valsup = " "
		aufnr = data["AUFNR"]
		objid = data["OBJID"]
		cusapp = data["CUSAPP"]
		obsup = data["OBSUP"]
		advance = data["ADVANCE"]
		otclosing = OTclosing.objects.filter(AUFNR=aufnr)
		datime = datetime.now()
		_date = str(datime).split('T')
		date = _date[0].split(' ')
		for o in otclosing:
			o.CUSAPP = cusapp
			o.OBSUP = obsup
			o.ADVANCE = advance
			o.RESPSUP = respsup
			o.VALSUP = valsup
			o.DATESUP = date[0]
			o.TIMESUP = date[1].split('.')[0]
			o.save()
		content = {'response': 'success'}
		return Response(content)


class OTIncidence(APIView):
	def post(self, request, format=None):
		_data = request.data.get("data")
		data = json.loads(_data)
		aufnr = data["AUFNR"]
		objid = data["OBJID"]
		obsi = data["OBSI"]
		othead = OThead.objects.get(AUFNR = aufnr, OBJID = objid)
		othead.OBSI = obsi
		othead.save()
		content = {'response': 'success'}
		return Response(content)


#Log
class LogAPIView(ListCreateAPIView):
	serializer_class = LogSerializer
	def get_queryset(self):
		return Log.objects.all()

class LogDetailAPIView(ListCreateAPIView):
	serializer_class = LogDetailSerializer
	def get_queryset(self):
		return LogDetail.objects.all()

class LogWebAPIView(APIView):
	def post(self, request, format=None):
		dti = datetime.strptime(request.data.get("dateini"), "%Y-%m-%d")
		dtf = datetime.strptime(request.data.get("datefin"), "%Y-%m-%d")
		logs = Log.objects.filter(DATEHOURSTART__range = [dti, drf])
		serializer = LogWebSerializer(logs, many = True)
		return Response(serializer.data)


class ResultSendWeb(APIView):
	def get(self, format = None):
		dt = str(datetime.now())
		_date = dt.split('T')
		date = _date[0].split(' ')
		print(str(datetime.now()))
		try:
			send = SendWeb.objets.get(DATE = date[0])
			countClos = 0
			countTicket = 0
			countSuccess = 0
			otheads = OThead.objects.all()
			otclos = OTclosing.objects.all()
			tickets = Ticket.objects.all()
			for ot in otheads:
				dt = str(ot.CREATED)
				_dat = dt.split('T')
				dat = _dat[0].split(' ')
				if dat[0] == date[0]:
					countSuccess = countSuccess + 1
			for o in otclos:
				try:
					dt = str(o.BEGDA)
				except:
					dt = str(o.ENDDA)
				_dat = dt.split('T')
				dat = _dat[0].split(' ')
				if dat[0] == date[0]:
					countClos = countClos + 1
			for t in tickets:
				dt = str(t.NDATE)
				_dat = dt.split('T')
				dat = _dat[0].split(' ')
				if dat[0] == date[0]:
					countTicket =countTicket + 1
			send.OTS = str(countSuccess)
			send.OTC = str(countClos)
			send.TIC = str(countTicket)
			send.save()
		except:
			countClos = 0
			countTicket = 0
			countSuccess = 0
			otheads = OThead.objects.all()
			otclos = OTclosing.objects.all()
			tickets = Ticket.objects.all()
			for ot in otheads:
				dt = str(ot.CREATED)
				_dat = dt.split('T')
				dat = _dat[0].split(' ')
				if dat[0] == date[0]:
					countSuccess = countSuccess +1
			for o in otclos:
				dt = str(o.ENDDA)
				_dat = dt.split('T')
				dat = _dat[0].split(' ')
				if dat[0] == date[0]:
					countClos = countClos + 1
			for t in tickets:
				dt = str(t.NDATE)
				_dat = dt.split('T')
				dat = _dat[0].split(' ')
				if dat[0] == date[0]:
					countTicket = countTicket + 1
			send = SendWeb(DATE = date[0], OTS = str(countSuccess), OTE = "0", OTC = str(countClos), TIC = str(countTicket))
			send.save()
		content = {'OTS': send.OTS, 'OTE': send.OTE, 'OTC': send.OTC, 'TIC':send.TIC}
		return Response(content)

class SendWebAPIView(APIView):
	def post(self, request, format=None):
		ote = request.data.get("OTE")
		dt = str(datetime.now())
		_date = dt.split('T')
		date = _date[0].split(' ')
		try:
			send = SendWeb.objects.get(DATE = date[0])
			if ote !=  "0":
				sum = int(send.OTE) + int(ote)
				send.OTE = str(sum)
			countClos = 0
			countTicket = 0
			countSuccess = 0
			otclos = OTclosing.objects.all()
			tickets = Ticket.objects.all()
			otheads = OThead.objects.all()
			for ot in otheads:
				dt = str(ot.CREATED)
				_dat = dt.split('T')
				dat = _dat[0].split(' ')
				if dat[0] == date[0]:
					countSuccess = countSuccess + 1
			for o in otclos:
				try:
					dt = str(o.BEGDA)
				except:
					dt = str(o.ENDDA)
				_dat = dt.split('T')
				dat = _dat[0].split(' ')
				if dat[0] == date[0]:
					countClos = countClos + 1
			for t in tickets:
				dt = str(t.NDATE)
				_dat = dt.split('T')
				dat = _dat[0].split(' ')
				if dat[0] == date[0]:
					countTicket = countTicket + 1
			send.OTC = str(countClos)
			send.TIC = str(countTicket)
			send.OTS = str(countSuccess)
			send.save()
		except:
			countSuccess = 0
			countClos = 0
			countTicket = 0
			otheads = OThead.objects.all()
			otclos = OTclosing.objects.all()
			tickets = Ticket.objects.all()
			for ot in otheads:
				dt = str(ot.CREATED)
				_dat = dt.split('T')
				dat = _dat[0].split(' ')
				if dat[0] == date[0]:
					countSuccess = countSuccess + 1
			for o in otclos:
				dt = str(o.ENDDA)
				_dat = dt.split('T')
				dat = _dat[0].split(' ')
				if dat[0] == date[0]:
					countClos = countClos + 1
			for t in tickets:
				dt = str(t.NDATE)
				_dat = dt.split('T')
				dat = _dat[0].split(' ')
				if dat[0] == date[0]:
					countTicket = countTicket + 1
			send = SendWeb(DATE = date[0], OTS = str(countSuccess), OTE = ote, OTC = str(countClos), TIC = str(countTicket))
			send.save()
		content = {'response': 'success'}
		return Response(content)

class SendGraficAPIWeb(APIView):
	def get(self, format = None):
		dt = str(datetime.now())
		__date = dt.split('T')
		_date = __date[0].split(' ')
		date = datetime.strptime(_date[0], '%Y-%m-%d')
		count = 14
		send = []
		for i in range(15):
			date2 = date - timedelta(days=i)
			dat2 = str(date2).split(' ')
			dt2 = dat2[0]
			countClos = 0
			countSend = 0
			otclosings = OTclosing.objects.filter(ENDDA = dt2)
			serializer = GraficSerializer(data={
					"DAY": str(dt2),
					"OTC": str(countClos),
					"OTE": str(countSend)
				})
			serializer.is_valid()
			send.append(serializer.data)
		return Response(send)

class Notification(APIView):
	def get(self, request, type, code, format=None):
		if type == "T":
			otoperations = OToperations.objects.filter(OBJID = code).values("AUFNR")
			otheads = OThead.objects.filter(AUFNR__in = otoperations, NOTIT = "0").exclude(NOTNR = "ESRP").exclude(NOTNR = "COMP").exclude(NOTNR = "ESSE")
			serializer = OTheadNotificationSerializer(otheads, many = True) 
		else:
			otheads = OThead.objects.filter(SOWRK = code, NOTIC = "0").values("AUFNR")
			otclos = OTclosing.objects.filter(AUFNR__in = otheads)
			serializer = OTclosingNotificationSerializer(otclos, many = True)
		return Response(serializer)

class GroupCenterAdmin(APIView):
	def post(self, request, format = None):
		center = request.data.get("centers")
		group = request.data.get("group")
		centers = center.split(',')
		groupplan = GroupPlanCenter.objects.filter(INGRP = group, IWERK__in = centers).values("IWERK")
		centers = Center.objects.filter(WERKS__in = groupplan)
		serializer = CenterAdminSerializer(centers, many = True)
		return Response({'data': serializer.data, 'response': 'success'})


class AdminFilter(APIView):
	def post(self, request, format = None):
		center = request.data.get("center")
		cusapp = request.data.get("cusapp")
		techapp = request.data.get("techapp")
		check = request.data.get("check")
		dti = datetime.strptime(request.data.get("dateini"), '%Y-%m-%d')
		dtf = datetime.strptime(request.data.get("datefin"), '%Y-%m-%d')
		code = request.data.get("technic")
		otheads = OThead.objects.filter(GSTRP__range = [dti, dtf])
		if center != "0":
			otheads = otheads.filter(SOWRK__in = center.split(","))
		if cusapp != "0":
			otclos = OTclosing.objects.filter(CUSAPP = cusapp).values("AUFNR")
			otheads = otheads.filter(AUFNR__in = otclos)
		if techapp != "0":
			otclos = OTclosing.objects.filter(TECHAPP = techapp).values("AUFNR")
			otheads = otheads.filter(AUFNR__in = otclos)
		if code != "0":
			otheads = otheads.filter(OBJID = code)
		serializer = OTheadAdminSerializer(otheads, many = True)
		return Response(serializer.data)

class AdminFilterDetail(APIView):
	def post(self, request, format = None):
		aufnr = request.data.get("AUFNR")
		othead = OThead.objects.get(AUFNR = aufnr)
		serializer = OTheadResumeSerializer(othead)
		return Response(serializer.data)

class DetailFilter(APIView):
	def post(self, request, format=None):
		aufnr = request.data.get("AUFNR")
		objid = request.data.get("OBJID")
		otoperations = OToperations.objects.filter(AUFNR = aufnr)
		sendO = OToperationsAdminSerializer(otoperations, many = True)
		otcomponents = OTcomponent.objects.filter(AUFNR = aufnr)
		sendC = OTcomponentAdminSerializer(otcomponents, many = True)
		otmeasures = OTmeasures.objects.filter(AUFNR = aufnr)
		sendM = OTmeasuresAdminSerializer(otmeasures, many = True)
		content = {'OTOPERATIONS': sendO.data, "OTCOMPONENTS": sendC.data, "OTMEASURES": sendM.data}
		return Response(content)

class MapService(APIView):
	def post(self, request, format=None):
		_data = request.data.get("data")
		print(_data)
		data = json.loads(_data)
		objid = data["OBJID"]
		lat = data["LAT"]
		lon = data["LON"]
		center = data["CENTER"]
		map = Map(OBJID = objid, LAT = lat, LON = lon, CENTER = center)
		map.save()
		content = {'response': 'success'}
		return Response(content)

class MapWebService(APIView):
	def get(self, request, format=None):
		maps = []
		jobs = JobPosition.objects.values_list("OBJID").distinct()
		for j in jobs:
			try:
				m = Map.objects.filter(OBJID = j[0]).latest('id')
				serializer = MapPositionSerializer(m)
				maps.append(serializer.data)				
			except:
				pass
		return Response({'response': 'success', 'data': maps})


class MapServiceDetail(APIView):
	def delete(self, request, pk, format = None):
		map = Map.objects.get(pk = pk)
		map.delete()
		content = {'response':'success'}
		return Response(content)
	def update(self, request, pk, format = None):
		map = Map.objects.get(pk = pk)
		objid = request.data.get("OBJID")
		lat = request.data.get("LAT")
		lon = request.data.get("LON")
		center = request.data.get("CENTER")
		map.OBJID = objid
		map.LAT = lat
		map.LON = lon
		map.CENTER = center
		map.save()
		content = {'response':'succcess'}
		return Response(content)

class PlanCenterService(APIView):
	def get(self, request, format=None):
		groups = Planning.objects.all()
		serializer = PlanningMobileSerializer(groups, many = True)
		return Response({'response':'success', 'data': serializer.data})

class Report(APIView):
	def get(self, request, format=None):
		response = HttpResponse(content_type='text/csv')
		response['Content-Disposition'] = 'attachment; filename="export.csv"'
		count = 0
		send = '['
		groups = Planning.objects.all()
		serializer = PlanningMobileSerializer(groups, many = True)
		header = ('INGRP', 'NAME', )
		writer = csv.DictWriter(response, fieldnames = header)
		writer.writeheader()
		for row in serializer.data:
			writer.writerow(row)
		return response



class ReportOT(APIView):
	def post(self, request, format=None):
		response = HttpResponse(content_type='application/ms-excel')
		response['Content-Disposition'] = 'attachment; filename="report_OT.xls"'
		columns = ['Numero de OT', 'Ultima fecha de actualizacion de SAP', 'Ultima Hora de actualizacion de SAP', 'Clase de Orden', 'Grupo de Planificacion', 'Centro de Emplazamiento', 'Texto de OT', 'Fecha de Inicio Extrema', 'Fecha de Fin Extrema', 'Puesto de Trabajo', 'Centro de Puesto de Trabajo', 'Ubicacion', 'Equipo', 'Cantidad de Operaciones', 'Cantidad de Componentes', 'Cantidad de Puntos de Medida', 'Cantidad de Tecnicos', 'Porcentaje de cierre', 'Fecha de Cierre Total', 'Hora de Cierre Total']
		aufnr = request.data.get("AUFNR")
		dateSAP = request.data.get("DATESAP")
		datePLAN = request.data.get("DATEPLAN")
		group = request.data.get("GROUP")
		center = request.data.get("CENTER")
		equip = request.data.get("EQUIP")
		close = request.data.get("CLOSE")
		dateini = datetime.strptime(dateSAP.split('_')[0], "%Y-%m-%d")
		datefin = datetime.strptime(dateSAP.split('_')[1], "%Y-%m-%d")
		otheads = OThead.objects.filter(CREATED__range=[dateini, datefin])
		serializer = OTheadReportSerializer(otheads, many = True)
		workbook = xlwt.Workbook()
		worksheet = workbook.add_sheet("Ordenes Recibidas")
		row_num = 0
		for col_num in range(len(columns)):
			worksheet.write(row_num, col_num, columns[col_num])
		for row in serializer.data:
			row_num = row_num + 1
			worksheet.write(row_num, 0, row["AUFNR"])
			worksheet.write(row_num, 1, row["UPDATE"])
			worksheet.write(row_num, 2, row["UPTIME"])
			worksheet.write(row_num, 3, row["ORDER"])
			worksheet.write(row_num, 4, row["GROUP"])
			worksheet.write(row_num, 5, row["WERKS"])
			worksheet.write(row_num, 6, row["KTEXT"])
			worksheet.write(row_num, 7, row["GSTRP"])
			worksheet.write(row_num, 8, row["GLTRP"])
			worksheet.write(row_num, 9, row["VAPLZ"])
			worksheet.write(row_num, 10, row["WAWRK"])
			worksheet.write(row_num, 11, row["TPLNR"])
			worksheet.write(row_num, 12, row["EQUNR"])
			worksheet.write(row_num, 13, row["COUNTOPER"])
			worksheet.write(row_num, 14, row["COUNTCOM"])
			worksheet.write(row_num, 15, row["COUNTMEA"])
			worksheet.write(row_num, 16, row["COUNTTEC"])
			worksheet.write(row_num, 17, row["ADVANCE"])
			worksheet.write(row_num, 18, row["CLOSEDATE"])
			worksheet.write(row_num, 19, row["CLOSETIME"])
		workbook.save(response)
		return response

class SuccessReport(APIView):
	def post(self, request, format=None):
		aufnr = request.data.get("AUFNR")
		dateSAP = request.data.get("DATESAP")
		datePLAN = request.data.get("DATEPLAN")
		group = request.data.get("GROUP")
		center = request.data.get("CENTER")
		equip = request.data.get("EQUIP")
		close = request.data.get("CLOSE")
		dateini = datetime.strptime(dateSAP.split('_')[0], "%Y-%m-%d")
		datefin = datetime.strptime(dateSAP.split('_')[1], "%Y-%m-%d")
		otheads = OThead.objects.filter(CREATED__range=[dateini, datefin])
		if aufnr != "0":
			otheads = OThead.objects.filter(AUFNR = aufnr)
		else:
			if center != "0":
				otheads = OThead.objects.filter(SOWRK = center)
			else:
				if group != "0":
					otheads = otheads.filter(INGRP = group)
				if datePLAN != "0":
					otheads = otheads.filter(GSTRP = datePLAN)
				if equip != "0":
					otheads = otheads.filter(EQUNR = equip)
		for o in otheads:
			try:
				otclos = OTclosing.objects.filter(AUFNR = o.AUFNR, OBJID = o.OBJID).first()
				if close == "si" or close == "todos":
					cl = 1
				else:
					cl = 0
			except:
				if close == "no" or close == "todos":
					cl = 1
				else:
					cl = 0
			if cl == 1:
				return Response({'response': 'success'})
		return Response({'response': 'empty'})


class ReportTechnic(APIView):
	def post(self, request, format=None):
		response = HttpResponse(content_type='application/ms-excel')
		response['Content-Disposition'] = 'attachment; filename="report_OT_header.xls"'
		aufnr = request.data.get("AUFNR")
		date = request.data.get("DATE")
		plan = request.data.get("GROUP")
		technic = request.data.get("TECHNIC")
		center = request.data.get("CENTER")
		equip = request.data.get("EQUIP")
		sap = request.data.get("SAP")
		approval = request.data.get("APPROVAL")
		dateini = datetime.strptime(date.split('_')[0],"%Y-%m-%d")
		datefin = datetime.strptime(date.split('_')[1],"%Y-%m-%d")
		if aufnr != "0":
			otheads = OThead.objects.filter(AUFNR = aufnr)
		else:
			otheads = OThead.objects.filter(CREATED__range=[dateini, datefin])
			if plan != "0":
				otheads = otheads.filter(INGRP = plan)
			if technic != "0":
				otheads = otheads.filter(VAPLZ = technic)
			if center != "0":
				otheads = otheads.filter(IWERK = center)
			if equip != "0":
				otheads = otheads.filter(EQUNR = equip)
		serializerClos = OTheadReportTechnicSerializer(otheads, many = True)
		otOperations = otheads.values("AUFNR")
		otoperations = COTOperations.objects.filter(AUFNR__in = otOperations)
		serializerOper = COTOperationsReportSerializer(otOperations, many = True)
		otcomponents = COTcomponent.objects.filter(AUFNR__in = otOperations)
		serializerCom = COTcomponentReportSerializer(otcomponents, many = True)
		otmeasures = COTmeasures.objects.filter(AUFNR__in = otOperations)
		serializerMea = COTmeasureReportSerializer(otmeasures, many = True)
		workbook = xlwt.Workbook()
		worksheet = workbook.add_sheet("Cabecera")
		workoper = workbook.add_sheet("Operaciones")
		workcom = workbook.add_sheet("Componentes")
		workmea = workbook.add_sheet("Puntos de Medida")
		row_num = 0
		columns = ['Numero de OT', 'Enviado a SAP', 'Codigo Tecnico', 'Nombre de Tecnico', 'Tecnico Responsable', 'Titulo de OT', 'Fecha de Inicio de OT', 'Hora de Inicio de OT', 'Fecha de Cierre de la OT', 'Hora de Cierre de OT', 'Centro', 'Nombre Centro', 'Ubicacion', 'Nombre Ubicacion', 'Equipo', 'Nombre Equipo', 'Duracion de OT', 'Aprobado Gerente', 'Fecha de Aprobacion Gerente', 'Hora de Aprobacion Gerente', 'Observacion de Gerente', 'Observacion de Tecnico', 'Satisfaccion de Tecnico', 'Observacion Satisfaccion de Tecnico', 'Satisfaccion Gerente', 'Observacion Satisfaccion de Gerente']
		for col_num in range(len(columns)):
			worksheet.write(row_num, col_num, columns[col_num])
		for row in serializerClos.data:
			row_num = row_num + 1
			worksheet.write(row_num, 0, row["AUFNR"])
			worksheet.write(row_num, 1, row["SEND"])
			worksheet.write(row_num, 2, row["VAPLZ"])
			worksheet.write(row_num, 3, row["JOBNAME"])
			worksheet.write(row_num, 4, "Si")
			worksheet.write(row_num, 5, row["KTEXT"])
			worksheet.write(row_num, 6, row["BEGDA"])
			worksheet.write(row_num, 7, row["BEGTI"])
			worksheet.write(row_num, 8, row["ENDDA"])
			worksheet.write(row_num, 9, row["ENDTI"])
			worksheet.write(row_num, 10, row["IWERK"])
			worksheet.write(row_num, 11, row["NAME1"])
			worksheet.write(row_num, 12, row["TPLNR"])
			worksheet.write(row_num, 13, row["PLTXT"])
			worksheet.write(row_num, 14, row["EQUNR"])
			worksheet.write(row_num, 15, row["EQKTX"])
			worksheet.write(row_num, 16, row["TIME"])
			worksheet.write(row_num, 17, row["APPR"])
			worksheet.write(row_num, 18, row["DATESUP"])
			worksheet.write(row_num, 19, row["TIMESUP"])
			worksheet.write(row_num, 20, row["OBSH"])
			worksheet.write(row_num, 21, row["OBSUP"])
			worksheet.write(row_num, 22, row["SATTEC"])
			worksheet.write(row_num, 23, row["VALTEC"])
			worksheet.write(row_num, 24, row["SATSUP"])
			worksheet.write(row_num, 25, row["VALSUP"])
		row_num = 0
		columns = ['Numero de Orden', 'Tecnico', 'Nombre Tecnico', 'Numero Tarea', 'Descripcion Tarea', 'Realizada', 'Horas invertidas', 'Minutos invertidos', 'Observacion']
		for col_num in range(len(columns)):
			workoper.write(row_num, col_num, columns[col_num])
		for row in serializerOper.data:
			row_num = row_num + 1
			workoper.write(row_num, 0, row["AUFNR"])
			workoper.write(row_num, 1, row["JOBCODE"])
			workoper.write(row_num, 2, row["JOBNAME"])
			workoper.write(row_num, 3, row["VORNR"])
			workoper.write(row_num, 4, row["LTXA1"])
			workoper.write(row_num, 5, row["DONE"])
			workoper.write(row_num, 6, row["OPTIME"])
			workoper.write(row_num, 7, row["OPTIME60"])
			workoper.write(row_num, 8, row["OPOBS"])
		row_num = 0
		columns = ['Numero de Orden', 'Tecnico', 'Nombre Tecnico', 'Codigo Componente', 'Componente', 'Unidad de Medida', 'Utilizado', 'Cantidad Utilizada']
		for col_num in range(len(columns)):
			workcom.write(row_num, col_num, columns[col_num])
		for row in serializerCom:
			row_num = row_num + 1
			workcom.write(row_num, 0, row["AUFNR"])
			workcom.write(row_num, 1, row["POSNR"])
			workcom.write(row_num, 2, row["JOBNAME"])
			workcom.write(row_num, 3, row["MATNR"])
			workcom.write(row_num, 4, row["MAKTX"])
			workcom.write(row_num, 5, row["MEINS"])
			workcom.write(row_num, 6, row["DONE"])
			workcom.write(row_num, 7, row["MENGE"])
		row_num = 0
		columns = ['Numero de Orden', 'Tecnico', 'Nombre de Tecnico', 'Equipo', 'Nombre de Equipo', 'Numero Punto', 'Desc. Punto', 'Valor Tomado', 'Valor']
		for col_num in range(len(columns)):
			workmea.write(row_num, col_num, columns[col_num])
		for row in serializerMea:
			row_num = row_num + 1
			workmea.write(row_num, 0, row["AUFNR"])
			workmea.write(row_num, 1, row["OBJID"])
			workmea.write(row_num, 2, row["JOBNAME"])
			workmea.write(row_num, 3, row["EQUNR"])
			workmea.write(row_num, 4, row["EQURNAME"])
			workmea.write(row_num, 5, row["POINT"])
			workmea.write(row_num, 6, row["PMOBS"])
			workmea.write(row_num, 7, row["DONE"])
			workmea.write(row_num, 8, row["VALUE"])
		workbook.save(response)
		return response

class SuccessReportTechnic(APIView):
	def post(self, request, format=None):
		aufnr = request.data.get("AUFNR")
		date = request.data.get("DATE")
		plan = request.data.get("GROUP")
		technic = request.data.get("TECHNIC")
		center = request.data.get("CENTER")
		equip = request.data.get("EQUIP")
		sap = request.data.get("SAP")
		approval = request.data.get("APPROVAL")
		dateini = datetime.strptime(date.split('_')[0], "%Y-%m-%d")
		datefin = datetime.strptime(date.split('_')[1], "%Y-%m-%d")
		if aufnr != "0":
			oheads = OThead.objects.filter(AUFNR = aufnr)
			date = "0"
			plan = "0"
			technic = "0"
			center = "0"
			equip = "0"
			sap = "Todo"
			approval = "Todo"
		else:
			otheads = OThead.objects.filter(CREATED__range=[dateini, datefin])
			if plan != "0":
				otheads = otheads.filter(INGRP = plan)
			if technic != "0":
				otheads = otheads.filter(VAPLZ = technic)
			if center != "0":
				otheads = otheads.filter(IWERK = center)
			if equip != "0":
				otheads = otheads.filter(EQUNR = equip)
		sendCLos = '['
		count = 0
		for ot in otheads:
			sp = 0
			ap = 0
			try:
				otclos = OTclosing.objects.get(AUFNR = ot.AUFNR, OBJID= ot.OBJID)
			except:
				pass
			if sap != "Todo":
				if otclos.STATUS == "0" and sap == "No":
					sp = 1
				if otclos.STATUS == "1" and sap == "Si":
					sp = 1
				if otclos.STATUS == "2" and sap == "Con Error":
					sp = 1
			else:
				sp = 1
			if approval != "Todo":
				if otclos.CUSAPP == "0" and approval == "No":
					ap = 1
				if otclos.CUSAPP == "1" and approval == "Si":
					ap = 1
			else:
				ap = 1
			if sp == 1 and ap == 1:
				return Response({'response': 'success'})
		return Response({'response': 'empty'})



class ReportTicket(APIView):
	def post(self, request, format=None):
		response = HttpResponse(content_type='application/ms-excel')
		response['Content-Disposition'] = 'attachment; filename="report_Ticket.xls"'
		columns = ['Numero de Ticket', 'Enviado a SAP', 'Estado de Ticket', 'Centro', 'Nombre de Centro', 'Usuario que lo crea', 'Fecha Creacion', 'Hora Creacion', 'Ubicacion', 'Desc. Ubicacion', 'Equipo', 'Nombre de Equipo', 'Sintoma', 'Desc. Sintoma', 'Estado Equipo', 'Observacion', 'Numero de OT SAP', 'Fecha Envio a SAP', 'Hora Envio SAP']
		ticnr = request.data.get("TICNR")
		aufnr = request.data.get("AUFNR")
		dates = request.data.get("DATES")
		plan = request.data.get("PLAN")
		center = request.data.get("CENTER")
		equip = request.data.get("EQUIP")
		sap = request.data.get("SAP")
		status = request.data.get("STATUS")
		if ticnr != "0":
			tickets = Ticket.objects.filter(id = int(ticnr))
		else:
			dateini = datetime.strptime(date.split('_')[0], "%Y-%m-%d")
			datefin = datetime.strptime(date.split('_')[1], "%Y-%m-%d")
			tickets = Ticket.objects.filter(NDATE__range = [dateini, datefin])
		if plan != "0":
			groupplan = GroupPlanCenter.objects.filter(INGRP = plan).values("IWERK")
			tickets = tickets.filter(WERKS__in = groupplan)
		if center != "0":
			tickets = tickets.filter(WERKS = center)
		if equip != "0":
			tickets = tickets.filter(EQUNR = equip)
		if sap != "Todo":
			if sap == "No":
				tickets = tickets.filter(STATUS = "0")
			if sap == "Si":
				tickets = tickets.filter(STATUS = "1")
			if sap == "Con Error":
				tickets = tickets.filter(STATUS = "2")
		if status != "Todo":
			if status == "Pendiente de Aprobar":
				tickets = tickets.filter(APPROVAL = "0")
			if status == "Aprobado":
				tickets = tickets.filter(APPROVAL = "1")
			if status == "Rechazado":
				tickets = tickets.filter(APPROVAL = "2")

		serializer = TicketSerializer(tickets, many = True)


		workbook = xlwt.Workbook()
		worksheet = workbook.add_sheet("Tickets")
		row_num = 0
		for col_num in range(len(columns)):
			worksheet.write(row_num, col_num, columns[col_num])
		for row in serializer.data:
			row_num = row_num + 1
			worksheet.write(row_num, 0, row["id"])
			worksheet.write(row_num, 1, row["STAT"])
			worksheet.write(row_num, 2, row["MSGPRO"])
			worksheet.write(row_num, 3, row["WERKS"])
			worksheet.write(row_num, 4, row["NWERKS"])
			worksheet.write(row_num, 5, row["WERKS"])
			worksheet.write(row_num, 6, row["DATE_CREATED"])
			worksheet.write(row_num, 7, row["TIME_CREATED"])
			worksheet.write(row_num, 8, row["TPLNR"])
			worksheet.write(row_num, 9, row["NTPLNR"])
			worksheet.write(row_num, 10, row["EQUNR"])
			worksheet.write(row_num, 11, row["NEQUNR"])
			worksheet.write(row_num, 12, row["CODE"])
			worksheet.write(row_num, 13, row["NEQUNR"])
			worksheet.write(row_num, 14, row["STOPPED"])
			worksheet.write(row_num, 15, row["REASON"])
			worksheet.write(row_num, 16, row["NUDOC"])
			worksheet.write(row_num, 17, row["FINI"])
			worksheet.write(row_num, 18, row["TINI"])
		workbook.save(response)
		return response

class SuccessReportTicket(APIView):
	def post(self, request, format=None):
		ticnr = request.data.get("TICNR")
		aufnr = request.data.get("AUFNR")
		dates = request.data.get("DATES")
		plan = request.data.get("PLAN")
		center = request.data.get("CENTER")
		equip = request.data.get("EQUIP")
		sap = request.data.get("SAP")
		status = request.data.get("STATUS")
		if ticnr != "0":
			tickets = Ticket.objects.filter(id = int(ticnr))
		else:
			if aufnr != "0":
				tickets = Ticket.objects.filter(NUDOC = aufnr)
			else:
				tickets = Ticket.objects.all()
		if ticnr != "0" or aufnr != "0":
			dates = "0"
			plan = "0"
			center = "0"
			equip = "0"
			sap = "Todo"
			status = "Todo"
		for t in tickets:
			dt = 0
			pl = 0
			cnt = 0
			equr = 0
			sp = 0
			st = 0
			if dates == "0":
				dt = 1
			else:
				dat = dates.split('_')
				if t.NDATE >= dat[0] and t.NDATE <= dat[1]:
					dt = 1
			if plan == "0":
				pl = 1
			else:
				try:
					groupplan = GroupPlanCenter.objects.get(INGRP = plan, IWERK = t.WERKS)
					pl = 1
				except:
					pl = 0

			if center != "0":
				if t.WERKS == center:
					cnt = 1
			else:
				cnt = 1

			if equip != "0":
				if t.EQUNR == equip:
					equr = 1
			else:
				equr = 1

			if sap != "Todo":
				if t.STATUS == "0" and sap == "No":
					sp = 1
				if t.STATUS == "1" and sap == "Si":
					sp = 1
				if t.STATUS == "2" and sap == "Con Error":
					sp = 1
			else:
				sp = 1

			if status != "Todo":
				if t.APPROVAL == "0" and status == "Pendiente de Aprobar":
					st = 1
				if t.APPROVAL == "1" and status == "Aprobado":
					st = 1
				if t.APPROVAL == "2" and status == "Rechazado":
					st = 1
			else:
				st = 1
			if dt == 1 and pl == 1 and cnt == 1 and equr == 1 and sp == 1 and st == 1:
				return Response({'response': 'success'})
		return Response({'response': 'empty'})

class EquipmentService(APIView):
	def get(self, request, center, format=None):
		equipments = Equipment.objects.filter(SWERK = center)
		serializer = EquipmentSerializer(equipments, many = True)
		return Response({'response': 'success', 'data': serializer.data})

