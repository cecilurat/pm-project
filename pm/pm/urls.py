from django.conf.urls import include, url
from django.contrib import admin
from django.contrib.staticfiles.views import serve
from rest_framework.authtoken import views
from django.views.generic import RedirectView
from accounts.views import UserAuthToken

api_urls = [
	url(r'^main/', include('main.urls', namespace='main')),
	url(r'main/mobility/', include('accounts.urls', namespace='mobility'))

]

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^api/', include(api_urls)),
#    url(r'^api-token-auth/', views.obtain_auth_token),
    url(r'^api-token-auth/', UserAuthToken.as_view(),  name="login"),

    url(r'^$', serve, kwargs={'path': 'index.html'}),
   # url(r'.*$', ser),
    url(r'^session/.*$', serve, kwargs={'path': 'index.html'}),
    url(r'^dashboard$', serve, kwargs={'path': 'index.html'}),
    url(r'^configuration/users-app$', serve, kwargs={'path': 'index.html'}),
    url(r'^masters/geolocation$', serve, kwargs={'path': 'index.html'}),
    url(r'^masters/types-services$', serve, kwargs={'path': 'index.html'}),
    url(r'^configuration/users-web$', serve, kwargs={'path': 'index.html'}),
    url(r'^configuration/.*$', serve, kwargs={'path':'index.html'}),
    url(r'^process/.*$', serve, kwargs={'path': 'index.html'}),
    url(r'^reports/.*$', serve, kwargs={'path': 'index.html'}),
    url(r'^download$', serve, kwargs={'path': 'index.html'}),
    url(r'^log$', serve, kwargs={'path': 'index.html'}),
    url(r'^(?!/?static/)(?!/?media/)(?P<path>.*\..*)$', RedirectView.as_view(url='/static/%(path)s', permanent=False)),

]

